//
//  Constants.swift
//  DairyLux
//
//  Created by Gabani King on 29/07/21.
//

import Foundation

//let BASE_URL = "http://app.sanghaniinfotech.com/api/"

let BASE_URL = "http://app.dhaamnatural.com/api/"

let IMG_BASE_URL = ""

let USER_EXIT = "User/userexists"

let USER_REGISTRATION = "User/registration"

let USER_CITY_LIST = "User/city_list"

let GET_REFERAL_CODE = "user/get_refercode"

let USER_SET_DELIVERY_ADDRESS = "User/setdelivery_address"

let CATEGORY_LIST = "Category/category_list"

let SUB_CATEGORY_LIST = "Category/subcategory_list"

let PRODUCT_LIST = "Category/product_list"

let PRODUCT_DETAILS = "Category/product_details"

let SAVE_ORDER = "order/save_order"

let GET_VACATION_MODE = "order/get_vacationmode"

let ADD_VACATION_MODE = "order/add_vacationmode"

let GET_PROFILE = "user/get_profile"

let UPDATE_PROFILE = "User/update_profile"

let UPDATE_DELIVERY_ADDRESS = "User/updatedelivery_address"

let APPLY_REFERAL_CODE = "User/apply_referalcode"

let GET_OFFER_LIST = "order/get_offerlist"

let UPDATE_VACATION_MODE = "order/update_vacationmode"

let GET_ORDER_LIST = "order/get_orderlist"

let ADD_WALLET_TRANSACTION = "account/addwallet_transaction"

let WALLET_BALANCE = "account/get_walletbalance"


let GET_ORDER_DETAILS = "order/get_orderdetails"


let appDelegate = UIApplication.shared.delegate as! AppDelegate

let PROVIDE_API_KEY = "AIzaSyDHTPV6rMVL5tVhstWL1PsAT_OTr0slzas"
