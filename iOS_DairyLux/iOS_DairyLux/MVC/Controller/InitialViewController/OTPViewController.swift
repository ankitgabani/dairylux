//
//  OTPViewController.swift
//  iOS_DairyLux
//
//  Created by Gabani King on 28/07/21.
//

import UIKit
import OTPFieldView
import Firebase
import FirebaseAuth
import Toast_Swift
import SVProgressHUD
class OTPViewController: UIViewController,responseDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var otpView: OTPFieldView!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var viewGardient: UIView!
    @IBOutlet weak var topImgBgHieghtCont: NSLayoutConstraint!
    
    var enteredOtp: String = ""
    var objMobile = ""
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            topImgBgHieghtCont.constant = 120
        }
        else
        {
            topImgBgHieghtCont.constant = 100
        }
        
        setUp()
        setupOtpView()
        
        gotoOTP()
        
   // callLoginAPI()
        // Do any additional setup after loading the view.
    }
      
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.viewGardient.layer.cornerRadius = 10
            self.viewGardient.layer.masksToBounds = true
            self.viewGardient.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    func setupOtpView(){
        self.otpView.fieldsCount = 6
        self.otpView.filledBackgroundColor = TEXT_BOX_COLOR
        self.otpView.defaultBackgroundColor = TEXT_BOX_COLOR
        self.otpView.cursorColor = PRIMARY_GARDIENT_1_COLOR
        self.otpView.filledBorderColor = UIColor.clear
        self.otpView.defaultBorderColor = UIColor.clear
        self.otpView.displayType = .roundedCorner
        self.otpView.fieldBorderWidth = 0
        self.otpView.fieldSize = 55
        self.otpView.separatorSpace = 2
        self.otpView.shouldAllowIntermediateEditing = false
        self.otpView.delegate = self
        self.otpView.fieldFont = UIFont(name: "NunitoSans-Bold", size: 22.0)!
        self.otpView.initializeUI()
    }
    
    func verifyOTP(){
        
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID ?? "",
                                                                 verificationCode: self.enteredOtp)
        Auth.auth().signIn(with: credential) { (authData, error) in
            
            if let error = error
            {
                self.view.makeToast("enter valid number.")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
            else
            {
                if let user1 = authData?.user {
                    print(user1.phoneNumber!)
                    
                    self.callLoginAPI()
                    
                }
            }
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedVerify(_ sender: Any) {
        verifyOTP()
    }
    
    @IBAction func clickedResned(_ sender: Any) {
        gotoOTP()
    }
    
    func gotoOTP()
    {
        
        SVProgressHUD.show()
        
        let phone = "+91" + objMobile
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error)
                self.view.makeToast(error.localizedDescription)
                return
            }else{
                //(toastText: "OTP Resend", withStatus: toastSuccess)
                
                SVProgressHUD.dismiss()
                
                self.view.makeToast("OTP Resend")
                print("OTP: \(verificationID ?? "")")
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    
    func callLoginAPI()
    {
        
        let dic = NSMutableDictionary()
        dic.setValue(objMobile, forKey: "mobile_no")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + USER_EXIT as NSString, type: .TYPE_POST_RAWDATA, ServiceName: USER_EXIT, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func callRegisterAPI()
    {
        
        let dic = NSMutableDictionary()
        dic.setValue(objMobile, forKey: "mobile_no")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(UIDevice.current.identifierForVendor?.uuidString, forKey: "device_id")
        dic.setValue("IOS", forKey: "device_type")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + USER_REGISTRATION as NSString, type: .TYPE_POST_RAWDATA, ServiceName: USER_REGISTRATION, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let data = dicData.value(forKey: "Data") as? NSArray
            let dicFirst = data?[0] as? NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            let type = dicFirst?.value(forKey: "type") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if ServiceName == USER_EXIT
            {
                if type == "1"
                {
                    let userid = dicFirst?.value(forKey: "userid") as? String
                    
                    UserDefaults.standard.setValue("\(userid ?? "")", forKey: "UserID")
                    UserDefaults.standard.setValue(true, forKey: "UserLogin")
                    UserDefaults.standard.synchronize()
                    self.callGetProfileAPI()
                }
                else
                {
                    self.callRegisterAPI()
                }
                
            }
            else if ServiceName == GET_PROFILE
            {
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    
                    let dicData = arrDicData?[0] as? NSDictionary
                    appDelegate.dicUserLoginData = dicData!
                    appDelegate.saveCurrentUserData(dic: dicData!)
                    
                    appDelegate.setUpSideMenu()
                }
              
            }
            else
            {
                let token = dicFirst?.value(forKey: "token") as? String
                appDelegate.saveCurrentUserToken(str: token ?? "")
                
                let userid = dicFirst?.value(forKey: "userid") as? Int
                
                UserDefaults.standard.setValue("\(userid ?? 0)", forKey: "UserID")
                UserDefaults.standard.setValue(true, forKey: "UserLogin")
                UserDefaults.standard.synchronize()
                
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "SelectCityViewController") as! SelectCityViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    func callGetProfileAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_PROFILE, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func didFinishWithFailure(ServiceName: String) {
        
    }
}

extension OTPViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        self.enteredOtp = otpString
    }
}
