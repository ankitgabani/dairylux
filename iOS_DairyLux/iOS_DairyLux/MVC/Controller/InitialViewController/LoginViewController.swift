//
//  LoginViewController.swift
//  iOS_DairyLux
//
//  Created by Gabani King on 26/07/21.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnNExt: UIButton!
    @IBOutlet weak var viewGardient: UIView!
    @IBOutlet weak var txtPhone: UITextField!
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp()
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUp()
    {
        bottomView.clipsToBounds = true
        bottomView.layer.cornerRadius = 20
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.viewGardient.layer.cornerRadius = 10
            self.viewGardient.layer.masksToBounds = true
            self.viewGardient.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    @IBAction func clickedNext(_ sender: Any) {
        
        if txtPhone.text == ""
        {
            self.view.makeToast("Please enter Mobile number")
        }
        else if txtPhone.text!.count > 11
        {
            self.view.makeToast("Please enter valid Mobile number")
        }
        else
        {
          //  gotoOTP()
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
            vc.objMobile = txtPhone.text ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    
    func gotoOTP()
    {
        let phone = "+91" + self.txtPhone.text!
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error)
                self.view.makeToast(error.localizedDescription)
                return
            }else{
                //(toastText: "OTP Resend", withStatus: toastSuccess)
                
                print("OTP: \(verificationID ?? "")")
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                UserDefaults.standard.synchronize()
                
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                vc.objMobile = self.txtPhone.text ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}

// MARK: - Gradient

public enum CAGradientPoint {
    case topLeft
    case centerLeft
    case bottomLeft
    case topCenter
    case center
    case bottomCenter
    case topRight
    case centerRight
    case bottomRight
    var point: CGPoint {
        switch self {
        case .topLeft:
            return CGPoint(x: 0, y: 0)
        case .centerLeft:
            return CGPoint(x: 0, y: 0.5)
        case .bottomLeft:
            return CGPoint(x: 0, y: 1.0)
        case .topCenter:
            return CGPoint(x: 0.5, y: 0)
        case .center:
            return CGPoint(x: 0.5, y: 0.5)
        case .bottomCenter:
            return CGPoint(x: 0.5, y: 1.0)
        case .topRight:
            return CGPoint(x: 1.0, y: 0.0)
        case .centerRight:
            return CGPoint(x: 1.0, y: 0.5)
        case .bottomRight:
            return CGPoint(x: 1.0, y: 1.0)
        }
    }
}

extension CAGradientLayer {
    
    convenience init(start: CAGradientPoint, end: CAGradientPoint, colors: [CGColor], type: CAGradientLayerType) {
        self.init()
        self.frame.origin = CGPoint.zero
        self.startPoint = start.point
        self.endPoint = end.point
        self.colors = colors
        self.locations = (0..<colors.count).map(NSNumber.init)
        self.type = type
    }
}

extension UIView {
    
    func layerGradient(startPoint:CAGradientPoint, endPoint:CAGradientPoint ,colorArray:[CGColor], type:CAGradientLayerType ) {
        let gradient = CAGradientLayer(start: .topLeft, end: .topRight, colors: colorArray, type: type)
        gradient.frame.size = self.frame.size
        self.layer.insertSublayer(gradient, at: 0)
    }
}
