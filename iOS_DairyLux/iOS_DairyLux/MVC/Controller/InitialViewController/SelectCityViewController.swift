//
//  SelectCityViewController.swift
//  iOS_DairyLux
//
//  Created by Gabani King on 28/07/21.
//

import UIKit

class SelectCityViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, responseDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var collectionViewCity: UICollectionView!
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewContine: UIView!
    
    var arrCity = NSMutableArray()
    var objCityID = ""
    
    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 18.0,
                                     bottom: 0.0,
                                     right: 18.0)
    let itemsPerRow: CGFloat = 3
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem + 20)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0.0, left: 18.0, bottom: 0.0, right: 18.0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 18.0
        _flowLayout.minimumLineSpacing = 18.0
        return _flowLayout
    }
    
    var isSelectedCity = -1
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        collectionViewCity.delegate = self
        collectionViewCity.dataSource = self
        
        collectionViewCity.collectionViewLayout = flowLayout
        // Do any additional setup after loading the view.
        setUp()
        
        callCityAPI()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.viewContine.layer.cornerRadius = 10
            self.viewContine.layer.masksToBounds = true
            self.viewContine.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    //MARK:- UICollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCity.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewCity.dequeueReusableCell(withReuseIdentifier: "SelectCityCollectionCell", for: indexPath) as! SelectCityCollectionCell
        
        let dicData = arrCity[indexPath.row] as? NSDictionary
        
        let name = dicData?.value(forKey: "name") as? String
        cell.lblCity.text = name ?? ""
        
//        if indexPath.row == 0
//        {
//            cell.imgPlace.isHidden = false
//            cell.imgCity.isHidden = true
//
//            cell.lblCity.isHidden = true
//            cell.imgCheckUncheck.isHidden = true
//        }
//        else
//        {
            cell.imgPlace.isHidden = true
            cell.imgCity.isHidden = false

            cell.lblCity.isHidden = false
            cell.imgCheckUncheck.isHidden = false
//        }
        
        if isSelectedCity == indexPath.item
        {
            cell.imgCheckUncheck.image = UIImage(named: "ic_check")
        }
        else
        {
            cell.imgCheckUncheck.image = UIImage(named: "ic_uncheck")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dicData = arrCity[indexPath.row] as? NSDictionary
        
        let id = dicData?.value(forKey: "id") as? String
        objCityID = id ?? ""
        
        self.isSelectedCity = indexPath.item
        self.collectionViewCity.reloadData()
    }
    
    @IBAction func clickedContunes(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ReferCodeViewController") as! ReferCodeViewController
        vc.objCityID = self.objCityID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callCityAPI()
    {
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + USER_CITY_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: USER_CITY_LIST, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")

            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else
            {
                let arrData = dicData.value(forKey: "Data") as? NSArray
                self.arrCity = (arrData?.mutableCopy() as? NSMutableArray)!
                self.collectionViewCity.reloadData()
            }
           
        }
    }
    
    func didFinishWithFailure(ServiceName: String) {
        
    }
}
