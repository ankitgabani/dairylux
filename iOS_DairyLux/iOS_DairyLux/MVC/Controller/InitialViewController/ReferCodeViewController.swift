//
//  ReferCodeViewController.swift
//  iOS_DairyLux
//
//  Created by Gabani King on 29/07/21.
//

import UIKit

class ReferCodeViewController: UIViewController, responseDelegate {
    
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewApplyCode: UIView!
    @IBOutlet weak var clickedCode: UITextField!
    
    var objCityID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.viewApplyCode.layer.cornerRadius = 10
            self.viewApplyCode.layer.masksToBounds = true
            self.viewApplyCode.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedApplyCode(_ sender: Any) {
        if clickedCode.text == ""
        {
            self.view.makeToast("Please enter refer code")
        }
        else
        {
            callApplyCodeAPI()
        }
    }
    
    @IBAction func clickedSkip(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DeliveryAddressVC") as! DeliveryAddressVC
        vc.objCityID = self.objCityID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func callApplyCodeAPI()
    {
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(clickedCode.text!, forKey: "refer_code")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + APPLY_REFERAL_CODE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: APPLY_REFERAL_CODE, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            let ReturnMessage = dicData.value(forKey: "ReturnMessage") as? String


            if token == "" || token == nil
            {
                self.view.makeToast(ReturnMessage)
            }
            else
            {
                appDelegate.saveCurrentUserToken(str: token ?? "")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DeliveryAddressVC") as! DeliveryAddressVC
                vc.objCityID = self.objCityID
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    func didFinishWithFailure(ServiceName: String) {
 
    }
}
