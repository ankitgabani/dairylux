//
//  DeliveryAddressVC.swift
//  DairyLux
//
//  Created by Gabani King on 29/07/21.
//

import UIKit
import GoogleMaps
import CoreLocation
import Alamofire
import GooglePlaces

class DeliveryAddressVC: UIViewController ,CLLocationManagerDelegate,GMSMapViewDelegate,UITextFieldDelegate, responseDelegate
{
    
    //MARK:- IBOutlet
    @IBOutlet weak var txtHouseNo: UITextField!
    @IBOutlet weak var txtDeliveryAddress: UITextField!
    @IBOutlet weak var txtLandMark: UITextField!
    @IBOutlet weak var btnAddress: UIButton!
    
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnWork: UIButton!
    @IBOutlet weak var btnOrther: UIButton!
    
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainView1: UIView!
    @IBOutlet weak var viewSaveAddress: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: GMSMapView!
    
    var objCityID = ""
    
    var isFromUpdateAddress = false
    
    let AddressViewHeightConst:CGFloat = 223
    
    var startPoint:CGFloat = 0.0
    var isUp:Bool = false
    var isSwipePrevent:Bool = true
    
    var locationManager = CLLocationManager()
    let marker: GMSMarker = GMSMarker() // Allocating Marker
    
    var objLatitude = ""
    var objLongitude = ""
    
    var strCountryID = ""
    var strCityID = ""
    var strCountryFlag = ""
    
    var strAddressLine1 = ""
    var strAddressLine2 = ""
    
    var addresstype = "1"
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        heightConstraint.constant = AddressViewHeightConst
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        let pangesture = UIPanGestureRecognizer(target: self, action: #selector(addressViewSpanGesture(_:)))
        self.mainView1.addGestureRecognizer(pangesture)
        
        mapSetUp()
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isFromUpdateAddress == true
        {
            callGetAddressAPI()
            btnAddress.setTitle("Update Address", for: .normal)
        }
        else
        {
            btnAddress.setTitle("Save Address", for: .normal)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
    }
    
    //MARK:- User Fucntion
    func setUp()
    {
        
        btnHome.backgroundColor = PRIMARY_GARDIENT_1_COLOR
        btnHome.setTitleColor(UIColor.white, for: .normal)
        
        btnWork.backgroundColor = UIColor.white
        btnWork.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        btnWork.layer.borderWidth = 1
        btnWork.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        btnOrther.backgroundColor = UIColor.white
        btnOrther.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        btnOrther.layer.borderWidth = 1
        btnOrther.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        //  mainView1.clipsToBounds = true
        mainView1.layer.cornerRadius = 25
        mainView1.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.viewSaveAddress.layer.cornerRadius = 10
            self.viewSaveAddress.layer.masksToBounds = true
            self.viewSaveAddress.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    //MARK:- Action Method
    
    @IBAction func clickeeBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSearch(_ sender: Any) {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    
    @IBAction func clickedHome(_ sender: Any) {
        addresstype = "1"
        btnHome.backgroundColor = PRIMARY_GARDIENT_1_COLOR
        btnHome.setTitleColor(UIColor.white, for: .normal)
        
        btnWork.backgroundColor = UIColor.white
        btnWork.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        btnWork.layer.borderWidth = 1
        btnWork.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        btnOrther.backgroundColor = UIColor.white
        btnOrther.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        btnOrther.layer.borderWidth = 1
        btnOrther.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
    }
    
    @IBAction func clickedWork(_ sender: Any) {
        addresstype = "2"
        btnWork.backgroundColor = PRIMARY_GARDIENT_1_COLOR
        btnWork.setTitleColor(UIColor.white, for: .normal)
        
        btnHome.backgroundColor = UIColor.white
        btnHome.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        btnHome.layer.borderWidth = 1
        btnHome.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        btnOrther.backgroundColor = UIColor.white
        btnOrther.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        btnOrther.layer.borderWidth = 1
        btnOrther.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
    }
    
    @IBAction func clickedOther(_ sender: Any) {
        addresstype = "3"
        btnOrther.backgroundColor = PRIMARY_GARDIENT_1_COLOR
        btnOrther.setTitleColor(UIColor.white, for: .normal)
        
        btnWork.backgroundColor = UIColor.white
        btnWork.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        btnWork.layer.borderWidth = 1
        btnWork.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        btnHome.backgroundColor = UIColor.white
        btnHome.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        btnHome.layer.borderWidth = 1
        btnHome.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
    }
    
    @IBAction func clickedCurrentLocation(_ sender: Any) {
        mapSetUp()
    }
    
    @IBAction func clickedSaveAddress(_ sender: Any) {
        
        if isFromUpdateAddress == true
        {
            callUpdateAddressAPI()
        }
        else
        {
            callAddAddressAPI()
        }
        
    }
    
    
    func mapSetUp()
    {
        self.mapView.delegate = self
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    //MARK:- Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        self.mapView.isMyLocationEnabled = false
        
        marker.icon = UIImage(named: "ic_location_pin") // Marker icon
        marker.position = location!.coordinate // CLLocationCoordinate2D
        
        DispatchQueue.main.async { // Setting marker on mapview in main thread.
            self.marker.map = self.mapView // Setting marker on Mapview
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
        self.objLatitude = String((location?.coordinate.latitude)!)
        self.objLongitude = String((location?.coordinate.longitude)!)
        
        self.mapView?.animate(to: camera)
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        
        getAddressFromLatLong(pdblLatitude: String((location?.coordinate.latitude)!), withLongitude: String((location?.coordinate.longitude)!))
        
        
        self.locationManager.stopUpdatingLocation()
    }
    
    
    func getAddressFromLatLong(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(lon)&key=\(PROVIDE_API_KEY)"
        
        AF.request(url).validate().responseJSON { response in
            switch response.result {
            case .success:
                
                let responseJson = response.value! as! NSDictionary
                
                if let results = responseJson.object(forKey: "results")! as? [NSDictionary] {
                    if results.count > 0 {
                        if let addressComponents = results[0]["address_components"]! as? [NSDictionary] {
                            
                            let address = results[0]["formatted_address"] as? String
                            let arrAddress = address?.components(separatedBy: ",") as! NSArray
                            print("Full Address: \(address))")
                            
                            if address!.count > 0{
                                if arrAddress.count >= 6{
                                    let address1 = arrAddress.object(at: 0) as? String
                                    let address2 = arrAddress.object(at: 1) as? String
                                    let address3 = arrAddress.object(at: 2) as? String
                                    let address4 = arrAddress.object(at: 3) as? String
                                    let address5 = arrAddress.object(at: 4) as? String
                                    let address6 = arrAddress.object(at: 5) as? String
                                    
                                    self.txtHouseNo.text = address1 ?? ""
                                    self.txtLandMark.text = address3 ?? ""
                                    
                                    self.txtDeliveryAddress.text = "\(address2 ?? ""), \(address3 ?? ""), \(address4 ?? ""), \(address5 ?? ""), \(address6 ?? "")"
                                }
                                else if arrAddress.count >= 5{
                                    let address1 = arrAddress.object(at: 0) as? String
                                    let address2 = arrAddress.object(at: 1) as? String
                                    let address3 = arrAddress.object(at: 2) as? String
                                    let address4 = arrAddress.object(at: 3) as? String
                                    let address5 = arrAddress.object(at: 4) as? String
                                    
                                    self.txtHouseNo.text = address1 ?? ""
                                    self.txtLandMark.text = address3 ?? ""
                                    
                                    self.txtDeliveryAddress.text = "\(address2 ?? ""), \(address3 ?? ""), \(address4 ?? ""), \(address5 ?? ""))"
                                }
                                else if arrAddress.count >= 4{
                                    let address1 = arrAddress.object(at: 0) as? String
                                    let address2 = arrAddress.object(at: 1) as? String
                                    let address3 = arrAddress.object(at: 2) as? String
                                    let address4 = arrAddress.object(at: 3) as? String
                                    
                                    self.txtHouseNo.text = address1 ?? ""
                                    self.txtLandMark.text = address3 ?? ""
                                    
                                    self.txtDeliveryAddress.text = "\(address2 ?? ""), \(address3 ?? ""), \(address4 ?? ""))"
                                }
                                else if arrAddress.count >= 3{
                                    let address1 = arrAddress.object(at: 0) as? String
                                    let address2 = arrAddress.object(at: 1) as? String
                                    let address3 = arrAddress.object(at: 2) as? String
                                    
                                    self.txtHouseNo.text = address1 ?? ""
                                    self.txtLandMark.text = address3 ?? ""
                                    
                                    self.txtDeliveryAddress.text = "\(address2 ?? ""), \(address3 ?? ""))"
                                }
                                else if arrAddress.count >= 3{
                                    let address1 = arrAddress.object(at: 0) as? String
                                    let address2 = arrAddress.object(at: 1) as? String
                                    
                                    self.txtHouseNo.text = address1 ?? ""
                                    self.txtLandMark.text = address2 ?? ""
                                    
                                    self.txtDeliveryAddress.text = "\(address2 ?? ""))"
                                }
                                else{
                                    let address1 = arrAddress.object(at: 0) as? String
                                    
                                    self.txtHouseNo.text = address1 ?? ""
                                    
                                    self.txtDeliveryAddress.text = "\(address1 ?? ""))"
                                }
                            }
                            
                        }
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @objc func addressViewSpanGesture(_ gesture:UIGestureRecognizer) {
        let pointY = gesture.location(in: self.view).y
        if gesture.state == .began {
            self.startPoint = pointY
        }
        else if gesture.state == .ended {
            if self.isSwipePrevent {
                return
            }
            self.startPoint = 0.0
            if self.isUp == false {
                self.isUp = true
                UIView.animate(withDuration: 0.5, animations: {
                    self.heightConstraint.constant = 500
                    self.view.layoutIfNeeded()
                })
            }
            else {
                self.view.endEditing(true)
                self.isUp = false
                UIView.animate(withDuration: 0.5, animations: {
                    self.heightConstraint.constant = self.AddressViewHeightConst
                    self.view.layoutIfNeeded()
                })
            }
        }
        else if gesture.state == .changed {
            if self.isUp == false {
                
                let diff = self.startPoint - pointY
                if diff > 0 {
                    self.isSwipePrevent = false
                    let totalwidth = CGFloat(500)
                    let constant = AddressViewHeightConst + diff
                    if constant <= totalwidth {
                        UIView.animate(withDuration: 0.5, animations: {
                            self.heightConstraint.constant = constant
                            self.view.layoutIfNeeded()
                        })
                    }
                }
                else {
                    //"Swipe down"
                    self.isSwipePrevent = true
                }
            }
            else {
                self.view.endEditing(true)
                
                let diff = pointY - self.startPoint
                if diff > 0 {
                    self.isSwipePrevent = false
                    let totalheight = CGFloat(500)
                    let constant = totalheight - diff
                    if constant >= AddressViewHeightConst {
                        UIView.animate(withDuration: 0.5, animations: {
                            self.heightConstraint.constant = constant
                            self.view.layoutIfNeeded()
                        })
                    }
                }
                else {
                    //"Swipe down"
                    self.isSwipePrevent = true
                }
            }
        }
        
    }
    
    func callGetAddressAPI()
    {
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_PROFILE, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func callAddAddressAPI()
    {
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(txtHouseNo.text ?? "", forKey: "address1")
        dic.setValue(txtDeliveryAddress.text ?? "", forKey: "address2")
        dic.setValue(txtLandMark.text ?? "", forKey: "address3")
        dic.setValue(objCityID, forKey: "cityid")
        dic.setValue("395004", forKey: "zipcode")
        dic.setValue(addresstype, forKey: "addresstype")
        dic.setValue(objLatitude, forKey: "latitude")
        dic.setValue(objLongitude, forKey: "longitude")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + USER_SET_DELIVERY_ADDRESS as NSString, type: .TYPE_POST_RAWDATA, ServiceName: USER_SET_DELIVERY_ADDRESS, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func callUpdateAddressAPI()
    {
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(txtHouseNo.text ?? "", forKey: "address1")
        dic.setValue(txtDeliveryAddress.text ?? "", forKey: "address2")
        dic.setValue(txtLandMark.text ?? "", forKey: "address3")
        dic.setValue(objCityID, forKey: "cityid")
        dic.setValue("395004", forKey: "zipcode")
        dic.setValue(addresstype, forKey: "addresstype")
        dic.setValue(objLatitude, forKey: "latitude")
        dic.setValue(objLongitude, forKey: "longitude")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + UPDATE_DELIVERY_ADDRESS as NSString, type: .TYPE_POST_RAWDATA, ServiceName: UPDATE_DELIVERY_ADDRESS, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            let ReturnMessage = dicData.value(forKey: "ReturnMessage") as? String

            if token == "" || token == nil
            {
                self.view.makeToast(ReturnMessage)
                appDelegate.setUpLoginData()
            }
            else if ServiceName == GET_PROFILE
            {
                appDelegate.saveCurrentUserToken(str: token ?? "")
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    let dicData = arrDicData?[0] as? NSDictionary
                    
                    let address1 = dicData?.value(forKey: "address1") as? String
                    let address2 = dicData?.value(forKey: "address2") as? String
                    let address3 = dicData?.value(forKey: "address3") as? String
                    let city = dicData?.value(forKey: "city") as? String
                    let zipcode = dicData?.value(forKey: "zipcode") as? String
                    
                    self.objCityID = city ?? ""
                    
                    self.txtHouseNo.text = address1
                    self.txtDeliveryAddress.text = address2
                    self.txtLandMark.text = address3
                }
                
                
            }
            else if ServiceName == USER_SET_DELIVERY_ADDRESS
            {
                appDelegate.saveCurrentUserToken(str: token ?? "")
                appDelegate.setUpSideMenu()
            }
            else if ServiceName == UPDATE_DELIVERY_ADDRESS
            {
                appDelegate.saveCurrentUserToken(str: token ?? "")
                
                self.view.makeToast("Address Successfully Updated")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        }
    }
    
    func didFinishWithFailure(ServiceName: String) {
 
    }
}
extension UIScreen {
    static var width:CGFloat {
        return UIScreen.main.bounds.width
    }
    static var height:CGFloat {
        return UIScreen.main.bounds.height
    }
}

extension DeliveryAddressVC: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    print("Place name: \(place.name ?? "")")
    print("Place address: \(place.formattedAddress ?? "")")
    self.txtDeliveryAddress.text = place.formattedAddress ?? ""
    print("Place attributions: \(place.attributions)")
    dismiss(animated: true, completion: nil)
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: \(error)")
    dismiss(animated: true, completion: nil)
  }

  // User cancelled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    print("Autocomplete was cancelled.")
    dismiss(animated: true, completion: nil)
  }
}
