//
//  SelectCityCollectionCell.swift
//  iOS_DairyLux
//
//  Created by Gabani King on 28/07/21.
//

import UIKit

class SelectCityCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCity: UIImageView!
    @IBOutlet weak var imgPlace: UIImageView!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var imgCheckUncheck: UIImageView!
    
    
}
