//
//  OfferVC.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 20/08/21.
//

import UIKit

class OfferVC: UIViewController, responseDelegate {
    
    @IBOutlet var topImgHeightCont: NSLayoutConstraint!
    
    @IBOutlet var collView: UICollectionView!
    @IBOutlet var mainView: UIView!
    
    var arrOfferList = NSMutableArray()
    
    var flowlayoutAuto: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 165, height: 150)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        _flowLayout.minimumInteritemSpacing = 5.0
        _flowLayout.minimumLineSpacing = 15.0
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        
        return _flowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            topImgHeightCont.constant = 120
        }
        else
        {
            topImgHeightCont.constant = 100
        }
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 25
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        callOfferListAPI()
        collView.delegate = self
        collView.dataSource = self
        collView.collectionViewLayout = flowlayoutAuto
    }
   
    
    @IBAction func TapOnBack(_ sender: Any) {
        appDelegate.setUpSideMenu()
    }
    
    
    func callOfferListAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_OFFER_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_OFFER_LIST, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            let ReturnMessage = dicData.value(forKey: "ReturnMessage") as? String
            
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == GET_OFFER_LIST
            {
                self.arrOfferList.removeAllObjects()
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    self.arrOfferList = (arrDicData?.mutableCopy() as? NSMutableArray)!
                }
                
                self.collView.reloadData()
            }
        }
    }
    
}


extension OfferVC: UICollectionViewDelegate, UICollectionViewDataSource
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfferList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collView.dequeueReusableCell(withReuseIdentifier: "OfferCell", for: indexPath) as! OfferCell
        
        let dicData = arrOfferList[indexPath.row] as? NSDictionary
        
        let descripat = dicData?.value(forKey: "descripation") as? String
        
        cell.lbltext.text = descripat
        
        return cell
    }
}
