//
//  OfferCell.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 20/08/21.
//

import UIKit

class OfferCell: UICollectionViewCell {

    @IBOutlet var cellView: UIView!
    @IBOutlet var lbltext: UILabel!
    @IBOutlet var imgCard: UIImageView!
  
}
