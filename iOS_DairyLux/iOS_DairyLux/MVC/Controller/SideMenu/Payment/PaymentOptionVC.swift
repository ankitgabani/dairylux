//
//  PaymentOptionVC.swift
//  DairyLux
//
//  Created by Gabani King on 06/08/21.
//

import UIKit
import Razorpay
import Alamofire

class PaymentOptionVC: UIViewController, RazorpayProtocol, RazorpayPaymentCompletionProtocolWithData {
    
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var mainView: UIView!
    
    var razorpay: RazorpayCheckout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        razorpay = RazorpayCheckout.initWithKey("rzp_test_tD2R2EIpRmohSh", andDelegateWithData: self)

        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedPay(_ sender: Any) {
        showPaymentForm()
    }
    
    public func showPaymentForm(){
        
        let options: [String:Any] = [
            "description": "test",
            "image": UIImage(named: "11favicon")!,
            "name": "Dhaam Natural",
            "amount" : 100,
            "prefill": [
                "contact": "7096859504",
                "email": "test@gmail.com"
            ],
            "theme": [
                "color": "#F37254"
          ]
            
        ]
        
        if let rzp = razorpay {
            rzp.open(options)
        } else {
            print("Unable to initialize")
        }
    }
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        print("error: ", code)
        //        let storyboard = UIStoryboard(name: "payment", bundle: nil)
        //        let razorpayVC = storyboard.instantiateViewController(withIdentifier: "paymentFailedViewController") as! paymentFailedViewController
        //        self.present(razorpayVC, animated: false, completion: nil)
    }
    
    
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        print("success: ", response)
        
        let paymentId = response?["razorpay_payment_id"] as! String
        
    }
    
}
