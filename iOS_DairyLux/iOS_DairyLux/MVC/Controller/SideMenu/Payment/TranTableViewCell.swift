//
//  TranTableViewCell.swift
//  Dhaam Natural
//
//  Created by Ankit Gabani on 17/03/22.
//

import UIKit

class TranTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTypr: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
