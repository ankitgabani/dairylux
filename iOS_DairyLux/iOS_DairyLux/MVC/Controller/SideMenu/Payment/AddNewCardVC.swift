//
//  AddNewCardVC.swift
//  DairyLux
//
//  Created by Gabani King on 06/08/21.
//

import UIKit

class AddNewCardVC: UIViewController {
    
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var txtCardNo: UITextField!
    @IBOutlet weak var lblCardName: UITextField!
    @IBOutlet weak var txtMMYY: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    
    @IBOutlet weak var viewPay: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.viewPay.layer.cornerRadius = 10
            self.viewPay.layer.masksToBounds = true
            self.viewPay.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedPay(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPaymentVC") as! SuccessPaymentVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
