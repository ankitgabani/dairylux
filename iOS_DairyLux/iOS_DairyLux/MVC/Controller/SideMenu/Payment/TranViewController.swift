//
//  TranViewController.swift
//  Dhaam Natural
//
//  Created by Ankit Gabani on 17/03/22.
//

import UIKit

class TranViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, responseDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var topImgHeightCont: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var arrOrderList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoData.isHidden = true
        tblView.delegate = self
        tblView.dataSource = self
        
        self.callGetWalletbalanceAPI()
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func setUp()
    {
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            topImgHeightCont.constant = 120
        }
        else
        {
            topImgHeightCont.constant = 100
        }
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 25
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
       
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
     }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelegate.setUpSideMenu()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "TranTableViewCell") as! TranTableViewCell
        
        let dicData = arrOrderList[indexPath.row] as? NSDictionary
        
        let creditamount = dicData?.value(forKey: "creditamount") as? String
        let debitamount = dicData?.value(forKey: "debitamount") as? String
        let transactiondate = dicData?.value(forKey: "transactiondate") as? String
        let transactiontype = dicData?.value(forKey: "transactiontype") as? String

        if transactiontype == "1"
        {
            cell.lblTypr.text = "Add wallet"
            
            let str_Round = Double(creditamount ?? "")?.rounded(toPlaces: 2)
            
            let str_Sring = String(str_Round ?? 0.0)

            cell.lblAmount.text = "+ ₹ \(str_Sring)"
            
            cell.lblAmount.textColor = UIColor(red: 96/255, green: 178/255, blue: 115/255, alpha: 1)

        }
        else if transactiontype == "2"
        {
            cell.lblTypr.text = "Refer amount"
            
            let str_Round = Double(creditamount ?? "")?.rounded(toPlaces: 2)
            
            let str_Sring = String(str_Round ?? 0.0)

            cell.lblAmount.text = "+ ₹ \(str_Sring)"

            cell.lblAmount.textColor = UIColor(red: 96/255, green: 178/255, blue: 115/255, alpha: 1)
        }
        else
        {
            cell.lblTypr.text = "Order"
            
            let str_Round = Double(debitamount ?? "")?.rounded(toPlaces: 2)
            
            let str_Sring = String(str_Round ?? 0.0)

            cell.lblAmount.text = "- ₹ \(str_Sring)"
            
            cell.lblAmount.textColor = .red

        }
        
        

//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd-MM-yyyy"
//
//        var dateFromString = ""
//        let arrSplit1 = transactiondate?.components(separatedBy: "-")
//        let strDay = arrSplit1?[0] as! String
//        let strMonth = arrSplit1?[1] as! String
//        let strYear = arrSplit1?[2] as! String
//        let monthNumber = (Int)(strMonth)
//        let fmt = DateFormatter()
//        fmt.dateStyle = .medium
//        let strMonthName = fmt.monthSymbols[monthNumber! - 1]
//        dateFromString = String.init(format: "%@ %@, %@ ", strDay,strMonthName.prefix(3) as CVarArg,strYear)
        
        cell.lblDate.text = transactiondate
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func callGetWalletbalanceAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + WALLET_BALANCE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: WALLET_BALANCE, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == WALLET_BALANCE
            {
                self.arrOrderList.removeAllObjects()
                
                var arrDicData = dicData.value(forKey: "Data") as? NSArray
                    
                if arrDicData!.count > 0
                {
                    arrDicData = arrDicData!.reversed() as NSArray
                    
                    if (arrDicData?.count ?? 0) > 0
                    {
                        self.arrOrderList = (arrDicData?.mutableCopy() as? NSMutableArray)!
                    }
                }
                
                if self.arrOrderList.count == 0
                {
                    self.lblNoData.isHidden = false
                }
                else
                {
                    self.lblNoData.isHidden = true
                }
               
                self.tblView.reloadData()
            }
            
        }
    }
}
