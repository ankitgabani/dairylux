//
//  SuccessPaymentVC.swift
//  DairyLux
//
//  Created by Gabani King on 06/08/21.
//

import UIKit

class SuccessPaymentVC: UIViewController {

    @IBOutlet var lbltitlt: UILabel!
    
    var isFromOrder = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromOrder == true
        {
            lbltitlt.text = "Your order created successfully."
        }

        // Do any additional setup after loading the view.
    }
    
 
    @IBAction func clickedHome(_ sender: Any) {
        appDelegate.setUpSideMenu()
    }
    
}
