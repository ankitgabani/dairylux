//
//  ReferVC.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 20/08/21.
//

import UIKit

class ReferVC: UIViewController,responseDelegate {
    
    //MARK:- IBOutlet
    
    @IBOutlet var topImgHeightCont: NSLayoutConstraint!

    
    @IBOutlet var lblReferralCode: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet var lblMoneyDes: UILabel!
    @IBOutlet var shareView: UIView!
    @IBOutlet var lblMoney: UILabel!
    
    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            topImgHeightCont.constant = 120
        }
        else
        {
            topImgHeightCont.constant = 100
        }
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 25
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        setUp()
        
        callGetCodeAPI()
    }
    
    //MARK:- Methods
    
    func setUp()
    {
        DispatchQueue.main.async {
            self.shareView.layer.cornerRadius = 10
            self.shareView.layer.masksToBounds = true
            self.shareView.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
   
    //MARK:- IBActions
    
    @IBAction func TapOnBack(_ sender: Any) {
        appDelegate.setUpSideMenu()
    }
    
    @IBAction func TapOnCopyCode(_ sender: Any) {
        UIPasteboard.general.string = lblReferralCode.text ?? ""
        
        self.view.makeToast("Copy Code")
    }
    
    @IBAction func TapOnShare(_ sender: Any) {
        
        if lblReferralCode.text ?? "" != ""
        {
            let activityViewController = UIActivityViewController(activityItems: [self.lblReferralCode.text ?? ""], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            
            present(activityViewController, animated: true, completion: nil)
        }else{
            print("There is nothing to share")
        }
        
    }
    
    //MARK:- API Call
    func callGetCodeAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_REFERAL_CODE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_REFERAL_CODE, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == GET_REFERAL_CODE
            {
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    
                    let dicData = arrDicData?[0] as? NSDictionary
                    
                    let referralcode = dicData?.value(forKey: "referralcode") as? String
                    
                    self.lblReferralCode.text = referralcode
                }
                
            }
        }
        
    }
    
}
