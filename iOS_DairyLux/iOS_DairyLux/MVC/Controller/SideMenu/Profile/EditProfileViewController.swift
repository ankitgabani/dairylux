//
//  EditProfileViewController.swift
//  DairyLux
//
//  Created by Gabani King on 07/08/21.
//

import UIKit

class EditProfileViewController: UIViewController, responseDelegate {
    
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewEdit: UIView!

    @IBOutlet weak var txtFullNAme: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPrimaryNumber: UITextField!
    
    @IBOutlet weak var txtAlterPhone: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
               
        callGetProfileAPI()
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        
        DispatchQueue.main.async {
            self.viewEdit.layer.cornerRadius = 10
            self.viewEdit.layer.masksToBounds = true
            self.viewEdit.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedEdit(_ sender: Any) {
        callUpdateProfileAPI()
    }
    
    //MARK:- API Call
    func callGetProfileAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_PROFILE, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func callGetProfileAPIzz()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: "GET_PROFILEZZ", bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func callUpdateProfileAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(txtFullNAme.text ?? "", forKey: "firstname")
        dic.setValue("", forKey: "lastname")
        dic.setValue(txtEmail.text ?? "", forKey: "email")
        dic.setValue(txtPrimaryNumber.text ?? "", forKey: "mobileno1")
        dic.setValue(txtAlterPhone.text ?? "", forKey: "mobileno2")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + UPDATE_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: UPDATE_PROFILE, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == UPDATE_PROFILE
            {
                self.view.makeToast("Profile Successfully Updated")
                
                self.callGetProfileAPIzz()
            }
            else if ServiceName == "GET_PROFILEZZ"
            {
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    
                    let dicData = arrDicData?[0] as? NSDictionary
                    appDelegate.dicUserLoginData = dicData!
                    appDelegate.saveCurrentUserData(dic: dicData!)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
              
            }
            else
            {
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    let dicData = arrDicData?[0] as? NSDictionary
                    
                    let first_name = dicData?.value(forKey: "first_name") as? String
                    let last_name = dicData?.value(forKey: "last_name") as? String
                 
                    let email = dicData?.value(forKey: "email") as? String
                    
                    let mobileno1 = dicData?.value(forKey: "mobileno1") as? String
                    let mobileno2 = dicData?.value(forKey: "mobileno2") as? String
                    
                    self.txtEmail.text = email
                    
                    self.txtFullNAme.text = "\(first_name ?? "") \(last_name ?? "")"
                    
                    self.txtPrimaryNumber.text = mobileno1
                    
                    self.txtAlterPhone.text = mobileno2
                }
                
                
            }
        }
    }
   
}
