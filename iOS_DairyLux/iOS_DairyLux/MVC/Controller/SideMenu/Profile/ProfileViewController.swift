//
//  ProfileViewController.swift
//  DairyLux
//
//  Created by Gabani King on 06/08/21.
//

import UIKit

class ProfileViewController: UIViewController,responseDelegate {
    
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var imgPlace: UIImageView!
    @IBOutlet weak var imgPro: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblRingBell: UILabel!
    @IBOutlet weak var lblVoice: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        callGetProfileAPI()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelegate.setUpSideMenu()
    }
    
    @IBAction func clickedEditProfile(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedEditAddress(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DeliveryAddressVC") as! DeliveryAddressVC
        vc.isFromUpdateAddress = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedEditDeliveryPre(_ sender: Any) {
        
    }
    
    //MARK:- API Call
    func callGetProfileAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_PROFILE, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == GET_PROFILE
            {
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    let dicData = arrDicData?[0] as? NSDictionary
                    
                    let first_name = dicData?.value(forKey: "first_name") as? String
                    let last_name = dicData?.value(forKey: "last_name") as? String
                    
                    let email = dicData?.value(forKey: "email") as? String
                    
                    let mobileno1 = dicData?.value(forKey: "mobileno1") as? String
                    let mobileno2 = dicData?.value(forKey: "mobileno2") as? String
                    
                    
                    self.lblName.text = "\(first_name ?? "") \(last_name ?? "")"
                    self.lblEmail.text = email
                    
                    if mobileno2 == "" || mobileno2 == nil
                    {
                        self.lblPhone.text = "\(mobileno1 ?? "")"
                    }
                    else
                    {
                        self.lblPhone.text = "\(mobileno1 ?? "")/\(mobileno2 ?? "")"
                    }
                    
                    let address1 = dicData?.value(forKey: "address1") as? String
                    let address2 = dicData?.value(forKey: "address2") as? String
                    let address3 = dicData?.value(forKey: "address3") as? String
                    let city = dicData?.value(forKey: "city") as? String
                    let zipcode = dicData?.value(forKey: "zipcode") as? String
                    
                    self.lblAddress.text = "\(address1 ?? ""),\n\(address2 ?? ""),\(address3 ?? ""),\(city ?? ""),\(zipcode ?? "")"
                }
                
                
            }
            
            
        }
    }
}
