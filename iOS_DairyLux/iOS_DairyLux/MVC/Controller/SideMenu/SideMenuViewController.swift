//
//  SideMenuViewController.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 02/08/21.
//

import UIKit
import LGSideMenuController

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, responseDelegate {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var isSelectedIndex = 0
    
    var arrName = ["Home","Products","Order List","Vacations","Wallet","My Profile","Bills","Offers","Refer and Earn","Transaction List","Support","Legal","Logout"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) {
            self.callGetProfileAPI()
        }
        
        tblView.delegate = self
        tblView.dataSource = self
        
        //        self.viewMain.clipsToBounds = true
        //        self.viewMain.layer.cornerRadius = 30
        //        self.viewMain.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.setGradientBackground()
        }
        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setGradientBackground() {
        let colorTop =  PRIMARY_GARDIENT_2_COLOR.cgColor
        let colorBottom = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        gradientLayer.cornerRadius = 30
        gradientLayer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        self.viewMain.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        
        cell.lblName.text = arrName[indexPath.row]
        
        if isSelectedIndex == indexPath.row
        {
            cell.imgSelection.isHidden = false
        }
        else
        {
            cell.imgSelection.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isSelectedIndex = indexPath.row
        tblView.reloadData()
        
        if indexPath.row == 0
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 1
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 2
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "OrderListViewController") as! OrderListViewController
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 3
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "VacationViewController") as! VacationViewController
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 4
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 5
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 6
        {
            
        }
        else if indexPath.row == 7
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "OfferVC") as! OfferVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 8
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ReferVC") as! ReferVC
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 9
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "TranViewController") as! TranViewController
            navigation.viewControllers = [vc]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 10
        {
            
        }
        else if indexPath.row == 11
        {
            
        }
        else
        {
            let alert = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Logout", style: .destructive, handler: { action in
                
                UserDefaults.standard.setValue("", forKey: "UserID")
                UserDefaults.standard.setValue(false, forKey: "UserLogin")
                UserDefaults.standard.synchronize()
                appDelegate.saveCurrentUserToken(str: "")
                appDelegate.setUpLoginData()
                
            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
        
    }
    
    
    //MARK:- API Call
    func callGetProfileAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_PROFILE, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == GET_PROFILE
            {
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    
                    let dicData = arrDicData?[0] as? NSDictionary
                    appDelegate.dicUserLoginData = dicData!
                    appDelegate.saveCurrentUserData(dic: dicData!)
                    
                    
                    let first_name = dicData?.value(forKey: "first_name") as? String
                    let last_name = dicData?.value(forKey: "last_name") as? String

                    self.lblName.text = "\(first_name ?? "") \(last_name ?? "")"
                }
              
            }

            
        }
    }
}
