//
//  VacationsTableCell.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 06/08/21.
//

import UIKit

class VacationsTableCell: UITableViewCell {

    
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
