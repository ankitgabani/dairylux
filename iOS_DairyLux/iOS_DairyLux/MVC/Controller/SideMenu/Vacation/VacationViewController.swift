//
//  VacationViewController.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 06/08/21.
//

import UIKit

class VacationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,responseDelegate {
    
    
    @IBOutlet weak var lblTtoleVacation: UILabel!
    @IBOutlet weak var btnVacantion: UIButton!
    
    
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var viewAddVacation: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    
    var objWhichSelection = ""
    
    var isFromUpdateVacation = false
    var vacationID = ""
    
    var arrVacation = NSMutableArray()
    
    var isFromHome = false
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoData.isHidden = true
        viewBG.isHidden = true
        
        tblView.delegate = self
        tblView.dataSource = self
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        callGetVacationmodeAPI()
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        viewPopUp.clipsToBounds = true
        viewPopUp.layer.cornerRadius = 20
        viewPopUp.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.viewAddVacation.layer.cornerRadius = 10
            self.viewAddVacation.layer.masksToBounds = true
            self.viewAddVacation.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelegate.setUpSideMenu()
    }
    
    @IBAction func clickedAdd(_ sender: Any)
    {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            self.viewBG.isHidden = false
        }
        
        self.viewPopUp.slideIn(from: kFTAnimationBottom, in: self.viewPopUp.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
    }
    
    @IBAction func clickedClose(_ sender: Any)
    {
        isFromUpdateVacation = false
        btnVacantion.setTitle("Add Vacation", for: .normal)
        lblTtoleVacation.text = "Add Vacation"
        
        self.viewPopUp.slideOut(to: kFTAnimationBottom, in: self.viewPopUp.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewBG.isHidden = true
        }
    }
    
    @IBAction func clickedStartDate(_ sender: Any)
    {
        objWhichSelection = "Start_date"
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(Date())
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        present(selector, animated: true, completion: nil)
    }
    
    @IBAction func clickedEndDate(_ sender: Any)
    {
        let alert = UIAlertController(title: "", message: "End date", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Choose Specific Date", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            
            self.objWhichSelection = "end_date"
            let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
            selector.delegate = self
            selector.optionCurrentDate =  self.singleDate
            selector.optionCurrentDates = Set( self.multipleDates)
            selector.optionCurrentDateRange.setStartDate( self.multipleDates.first ??  self.singleDate)
            selector.optionCurrentDateRange.setEndDate( self.multipleDates.last ??  self.singleDate)
            selector.optionStyles.showDateMonth(true)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showYear(true)
            selector.optionStyles.showTime(false)
            self.present(selector, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Not Yet Decided", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
        }))
        
        //uncomment for iPad Support
        //alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @IBAction func clickedAddMoney(_ sender: Any)
    {
        if isFromUpdateVacation == true
        {
            isFromUpdateVacation = false
            btnVacantion.setTitle("Add Vacation", for: .normal)
            lblTtoleVacation.text = "Add Vacation"
            callUpdateVacationmodeAPI(id: self.vacationID, fromdate: txtStartDate.text ?? "", todate: txtEndDate.text ?? "")
        }
        else
        {
            callAddVacationmodeAPI()
        }
        
        self.viewPopUp.slideOut(to: kFTAnimationBottom, in: self.viewPopUp.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewBG.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVacation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "VacationsTableCell") as! VacationsTableCell
        
        let dicData = arrVacation[indexPath.row] as? NSDictionary
        
        let vacation_endtdate = dicData?.value(forKey: "vacation_endtdate") as? String
        let vacation_startdate = dicData?.value(forKey: "vacation_startdate") as? String

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
       
        var dateFromString = ""
        let arrSplit1 = vacation_startdate?.components(separatedBy: "-")
        let strDay = arrSplit1?[0] as! String
        let strMonth = arrSplit1?[1] as! String
        let strYear = arrSplit1?[2] as! String
        let monthNumber = (Int)(strMonth)
        let fmt = DateFormatter()
        fmt.dateStyle = .medium
        let strMonthName = fmt.monthSymbols[monthNumber! - 1]
        dateFromString = String.init(format: "%@ %@, %@ ", strDay,strMonthName.prefix(3) as CVarArg,strYear)
        cell.lblStartDate.text = dateFromString
        
        var dateFromString1 = ""
        let arrSplit11 = vacation_endtdate?.components(separatedBy: "-")
        let strDay1 = arrSplit11?[0] as! String
        let strMonth1 = arrSplit11?[1] as! String
        let strYear1 = arrSplit11?[2] as! String
        let monthNumber1 = (Int)(strMonth1)
        let fmt1 = DateFormatter()
        fmt1.dateStyle = .medium
        let strMonthName1 = fmt1.monthSymbols[monthNumber1! - 1]
        dateFromString1 = String.init(format: "%@ %@, %@ ", strDay1,strMonthName1.prefix(3) as CVarArg,strYear1)
        cell.lblEndDate.text = dateFromString1

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        isFromUpdateVacation = true
        
        btnVacantion.setTitle("Update Vacation", for: .normal)
        lblTtoleVacation.text = "Update Vacation"
        
        let dicData = arrVacation[indexPath.row] as? NSDictionary
        
        let id = dicData?.value(forKey: "id") as? String
        self.vacationID = id!
        
        let vacation_endtdate = dicData?.value(forKey: "vacation_endtdate") as? String
        let vacation_startdate = dicData?.value(forKey: "vacation_startdate") as? String

        self.txtStartDate.text = vacation_startdate
        self.txtEndDate.text = vacation_endtdate

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            self.viewBG.isHidden = false
        }
        
        self.viewPopUp.slideIn(from: kFTAnimationBottom, in: self.viewPopUp.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    //MARK:- API Call
    func callGetVacationmodeAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_VACATION_MODE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_VACATION_MODE, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func callUpdateVacationmodeAPI(id: String, fromdate: String, todate: String)
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(fromdate, forKey: "fromdate")
        dic.setValue(todate, forKey: "todate")
        dic.setValue("\(id)", forKey: "id")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + UPDATE_VACATION_MODE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: UPDATE_VACATION_MODE, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func callAddVacationmodeAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(txtStartDate.text ?? "", forKey: "fromdate")
        dic.setValue(txtEndDate.text ?? "", forKey: "todate")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + ADD_VACATION_MODE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: ADD_VACATION_MODE, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")

            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == GET_VACATION_MODE
            {
                self.arrVacation.removeAllObjects()
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray

                if (arrDicData?.count ?? 0) > 0
                {
                    self.arrVacation = (arrDicData?.mutableCopy() as? NSMutableArray)!
                }
                
                if self.arrVacation.count == 0
                {
                    self.lblNoData.isHidden = false
                }
                else
                {
                    self.lblNoData.isHidden = true
                }
                
                self.tblView.reloadData()
            }
            else if ServiceName == ADD_VACATION_MODE || ServiceName == UPDATE_VACATION_MODE
            {
                self.callGetVacationmodeAPI()
            }
            
        }
    }
}

extension VacationViewController: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        
        let PevDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        
        if date >= PevDate
        {
            return true
        }
        return false
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        
        if objWhichSelection == "Start_date"
        {
            txtStartDate.text = date.stringFromFormat("dd/MM/yyyy")
        }
        else if objWhichSelection == "end_date"
        {
            txtEndDate.text = date.stringFromFormat("dd/MM/yyyy")
        }
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            
            if objWhichSelection == "Start_date"
            {
                txtStartDate.text = date.stringFromFormat("dd/MM/yyyy")
            }
            else if objWhichSelection == "end_date"
            {
                txtEndDate.text = date.stringFromFormat("dd/MM/yyyy")
            }
        }
        else {
            
            if objWhichSelection == "Start_date"
            {
                txtStartDate.text = "No Date Selected"
                
            }
            else if objWhichSelection == "end_date"
            {
                txtEndDate.text = "No Date Selected"
            }
        }
        multipleDates = dates
    }
}
