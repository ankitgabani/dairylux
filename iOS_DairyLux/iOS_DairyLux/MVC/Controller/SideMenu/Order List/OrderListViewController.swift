//
//  OrderListViewController.swift
//  DairyLux
//
//  Created by Gabani King on 21/08/21.
//

import UIKit

class OrderListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, responseDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var topImgHeightCont: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var arrOrderList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoData.isHidden = true
        tblView.delegate = self
        tblView.dataSource = self
        callOrderListAPI()
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func setUp()
    {
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            topImgHeightCont.constant = 120
        }
        else
        {
            topImgHeightCont.constant = 100
        }
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 25
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
       
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
     }
    
  
    @IBAction func clickedBack(_ sender: Any) {
        appDelegate.setUpSideMenu()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "OrderListTableCell") as! OrderListTableCell
        
        let dicData = arrOrderList[indexPath.row] as? NSDictionary
        
        let orderno = dicData?.value(forKey: "orderno") as? String
        let orderdate = dicData?.value(forKey: "orderdate") as? String

        cell.lblOrderNo.text = orderno

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
       
        var dateFromString = ""
        let arrSplit1 = orderdate?.components(separatedBy: "-")
        let strDay = arrSplit1?[0] as! String
        let strMonth = arrSplit1?[1] as! String
        let strYear = arrSplit1?[2] as! String
        let monthNumber = (Int)(strMonth)
        let fmt = DateFormatter()
        fmt.dateStyle = .medium
        let strMonthName = fmt.monthSymbols[monthNumber! - 1]
        dateFromString = String.init(format: "%@ %@, %@ ", strDay,strMonthName.prefix(3) as CVarArg,strYear)
        cell.lblOrderDate.text = dateFromString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dicData = arrOrderList[indexPath.row] as? NSDictionary
        let strOrderID = dicData?.value(forKey: "id") as? String

        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let orderdetailsVC = storyBoard.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
        orderdetailsVC.strOrderID = strOrderID ?? ""
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(orderdetailsVC, animated: true)
    }


    
    func callOrderListAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_ORDER_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_ORDER_LIST, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            let ReturnMessage = dicData.value(forKey: "ReturnMessage") as? String
            
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == GET_ORDER_LIST
            {
                self.arrOrderList.removeAllObjects()
                
                var arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if arrDicData!.count > 0
                {
                    arrDicData = arrDicData!.reversed() as NSArray
                    
                    if (arrDicData?.count ?? 0) > 0
                    {
                        self.arrOrderList = (arrDicData?.mutableCopy() as? NSMutableArray)!
                    }
                                    
                }
                
                if self.arrOrderList.count == 0
                {
                    self.lblNoData.isHidden = false
                }
                else
                {
                    self.lblNoData.isHidden = true
                }
                self.tblView.reloadData()
            }
        }
    }
}
