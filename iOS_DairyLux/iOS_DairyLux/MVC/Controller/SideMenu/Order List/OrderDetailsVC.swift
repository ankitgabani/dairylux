//
//  OrderDetailsVC.swift
//  DairyLux
//
//  Created by Sahil Moradiya on 25/09/21.
//

import UIKit
import SDWebImage

class OrderDetailsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, responseDelegate {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var collectionViewProduct: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var lblProName: UILabel!
    @IBOutlet weak var lblNewPrice: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblCountMorning: UILabel!

    @IBOutlet weak var lblCountNight: UILabel!

    
    @IBOutlet weak var viewSelectMorning: UIView!
    @IBOutlet weak var viewSelectNight: UIView!
    
    @IBOutlet weak var viewDays: UIView!
    @IBOutlet weak var viewALter: UIView!
    @IBOutlet weak var viewSelectDays: UIView!
    
    @IBOutlet weak var btnDays: UIButton!
    @IBOutlet weak var btnALter: UIButton!
    @IBOutlet weak var btnSelectDays: UIButton!
    
    @IBOutlet weak var viewDaysHeightCont: NSLayoutConstraint!
    @IBOutlet weak var viewSun: UIView!
    @IBOutlet weak var imgSun: UIImageView!
    
    @IBOutlet weak var viewMon: UIView!
    @IBOutlet weak var imgMon: UIImageView!
    
    @IBOutlet weak var viewThe: UIView!
    @IBOutlet weak var imgThe: UIImageView!
    
    @IBOutlet weak var viewWed: UIView!
    @IBOutlet weak var imgWed: UIImageView!
    
    @IBOutlet weak var viewThu: UIView!
    @IBOutlet weak var imgThu: UIImageView!
    
    
    @IBOutlet weak var viewFri: UIView!
    @IBOutlet weak var imgFri: UIImageView!
    
    @IBOutlet weak var viewSat: UIView!
    @IBOutlet weak var imgSat: UIImageView!
    
    
    @IBOutlet weak var btnMorning: UIButton!
    @IBOutlet weak var btnNight: UIButton!
    
    @IBOutlet weak var txtDeliveryTime: UITextField!
    
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var txtEndData: UITextField!
    
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var viewConfirm: UIView!
        
    var dicOrderDetails = NSMutableDictionary()
    
    var strOrderID = ""
    
    var flowLayoutSlider: UICollectionViewFlowLayout {
        
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 280)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        
        return _flowLayout
    }
    
    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orederApiCall()
        
        viewDaysHeightCont.constant = 0
        pageController.isHidden = true
        collectionViewProduct.delegate = self
        collectionViewProduct.dataSource = self
        self.collectionViewProduct.collectionViewLayout = flowLayoutSlider
    }
    
    func setUp()
    {
        
        let qty = dicOrderDetails.value(forKey: "qty") as? String
     //   lblCount.text = qty
        
        let morning = dicOrderDetails.value(forKey: "morning") as? String
        
        if morning != "0"
        {
            viewSelectMorning.isHidden = false
            
            viewSelectMorning.isHidden = false
            lblCountMorning.text = morning
            
            btnMorning.backgroundColor = PRIMARY_GARDIENT_1_COLOR
            btnMorning.setTitleColor(UIColor.white, for: .normal)
        }
        else
        {
            
            viewSelectMorning.isHidden = true
            lblCountMorning.text = morning
            
            
            btnMorning.backgroundColor = UIColor.white
            btnMorning.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            btnMorning.layer.borderWidth = 1
            btnMorning.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        }
        
        
        let night = dicOrderDetails.value(forKey: "night") as? String

        if night != "0"
        {
            
            viewSelectNight.isHidden = false
            lblCountNight.text = night
            
            btnNight.backgroundColor = PRIMARY_GARDIENT_1_COLOR
            btnNight.setTitleColor(UIColor.white, for: .normal)
            
        }
        else
        {
            
            viewSelectNight.isHidden = true
            lblCountNight.text = night

            btnNight.backgroundColor = UIColor.white
            btnNight.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            btnNight.layer.borderWidth = 1
            btnNight.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        }

       
        
        let proname = dicOrderDetails.value(forKey: "productname") as? String
        lblProName.text = proname
        
        let oldprice = dicOrderDetails.value(forKey: "productprice") as? String
        let old_PriceZ = Double(oldprice ?? "")?.rounded(toPlaces: 2)
        
        let newprice = dicOrderDetails.value(forKey: "offer_price") as? String
        let new_PriceZ = Double(newprice ?? "")?.rounded(toPlaces: 2)
        lblNewPrice.text = "₹ \(new_PriceZ ?? 0.0)"
        
        let finalprice = Double(newprice ?? "")! * Double(Int(qty ?? "")!)
        lblTotalAmount.text = "₹ \(finalprice)"
        
     
        collectionViewProduct.reloadData()
        
        let startdate = dicOrderDetails.value(forKey: "order_startdate") as? String
        txtStartDate.text = startdate
        
        let enddate = dicOrderDetails.value(forKey: "order_enddate") as? String
        txtEndData.text = enddate
        
        let example1 = NSAttributedString(string: "₹")
        let example = NSAttributedString(string: " \(old_PriceZ ?? 0.0)").withStrikeThrough(1)
        
        let priceNSMutableAttributedString1 = NSMutableAttributedString()
        priceNSMutableAttributedString1.append(example1)
        priceNSMutableAttributedString1.append(example)
        lblOldPrice.attributedText =  priceNSMutableAttributedString1
        
//        let timeid = dicOrderDetails.value(forKey: "timeid") as? String
//        if timeid == "1"
//        {
//            btnMorning.backgroundColor = PRIMARY_GARDIENT_1_COLOR
//            btnMorning.setTitleColor(UIColor.white, for: .normal)
//            btnNight.backgroundColor = UIColor.white
//            btnNight.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
//            btnNight.layer.borderWidth = 1
//            btnNight.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
//        }
//        else if timeid == "2"
//        {
//            btnMorning.backgroundColor = UIColor.white
//            btnMorning.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
//            btnMorning.layer.borderWidth = 1
//            btnMorning.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
//            btnNight.backgroundColor = UIColor.white
//            btnNight.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
//            btnNight.layer.borderWidth = 1
//            btnNight.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
//        }
//        else
//        {
//            btnNight.backgroundColor = PRIMARY_GARDIENT_1_COLOR
//            btnNight.setTitleColor(UIColor.white, for: .normal)
//            btnMorning.backgroundColor = UIColor.white
//            btnMorning.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
//            btnMorning.layer.borderWidth = 1
//            btnMorning.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
//        }
        
        let day = dicOrderDetails.value(forKey: "datatypeid") as? String
        
        if day == "1"
        {
            viewDays.backgroundColor = PRIMARY_GARDIENT_1_COLOR
            btnDays.setTitleColor(UIColor.white, for: .normal)
            viewALter.backgroundColor = UIColor.white
            btnALter.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            viewALter.layer.borderWidth = 1
            viewALter.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
            viewSelectDays.backgroundColor = UIColor.white
            btnSelectDays.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            viewSelectDays.layer.borderWidth = 1
            viewSelectDays.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
            
            viewSun.isHidden = true
            viewMon.isHidden = true
            viewThe.isHidden = true
            viewWed.isHidden = true
            viewThu.isHidden = true
            viewFri.isHidden = true
            viewSat.isHidden = true
        }
        else if day == "2"
        {
            viewALter.backgroundColor = PRIMARY_GARDIENT_1_COLOR
            btnALter.setTitleColor(UIColor.white, for: .normal)
            viewDays.backgroundColor = UIColor.white
            btnDays.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            viewDays.layer.borderWidth = 1
            viewDays.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
            viewSelectDays.backgroundColor = UIColor.white
            btnSelectDays.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            viewSelectDays.layer.borderWidth = 1
            viewSelectDays.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
            
            viewSun.isHidden = true
            viewMon.isHidden = true
            viewThe.isHidden = true
            viewWed.isHidden = true
            viewThu.isHidden = true
            viewFri.isHidden = true
            viewSat.isHidden = true
        }
        else
        {
            viewDaysHeightCont.constant = 55.5
            viewSelectDays.backgroundColor = PRIMARY_GARDIENT_1_COLOR
            btnSelectDays.setTitleColor(UIColor.white, for: .normal)
            viewALter.backgroundColor = UIColor.white
            btnALter.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            viewALter.layer.borderWidth = 1
            viewALter.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
            viewDays.backgroundColor = UIColor.white
            btnDays.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            viewDays.layer.borderWidth = 1
            viewDays.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
            
            viewSun.isHidden = false
            viewMon.isHidden = false
            viewThe.isHidden = false
            viewWed.isHidden = false
            viewThu.isHidden = false
            viewFri.isHidden = false
            viewSat.isHidden = false
            
            if let selectday = dicOrderDetails.value(forKey: "selectedday") as? String
            {
                let arrselectday = selectday.components(separatedBy: ",")
                
                for objDay in arrselectday
                {
                    
                    if objDay == "1"
                    {
                        imgSun.image = UIImage(named: "checkbox_active")
                    }

                    if objDay == "2"
                    {
                        imgMon.image = UIImage(named: "checkbox_active")
                    }

                    if objDay == "3"
                    {
                        imgThu.image = UIImage(named: "checkbox_active")
                    }
                    if objDay == "4"
                    {
                        imgWed.image = UIImage(named: "checkbox_active")
                    }
                    if objDay == "5"
                    {
                        imgThe.image = UIImage(named: "checkbox_active")
                    }
                    if objDay == "6"
                    {
                        imgFri.image = UIImage(named: "checkbox_active")
                    }
                    if objDay == "7"
                    {
                        imgSat.image = UIImage(named: "checkbox_active")
                    }
                }
                
            }
        }
    }
    
    
    
    //MARK:- IBActions
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedNewAdd(_ sender: Any) {
    }
    
    @IBAction func clickedAdded(_ sender: Any)
    {
        
        
    }
    
    @IBAction func clickedSubed(_ sender: Any)
    {
        
    }
    
    @IBAction func clickedDays(_ sender: Any) {
        
    }
    
    @IBAction func clickedAlter(_ sender: Any)
    {
        
    }
    
    @IBAction func clickedSelectDays(_ sender: Any) {
        
    }
    
    @IBAction func clickedMorning(_ sender: Any) {
        
        
    }
    
    @IBAction func clickedAfternoon(_ sender: Any) {
        
        
    }
    
    @IBAction func clickedNight(_ sender: Any) {
        
        
    }
    
    @IBAction func clickedSelecteDivliveryTine(_ sender: Any)
    {
    }
    
    @IBAction func clickedSelectEndDate(_ sender: Any)
    {
        
        
        
    }
    
    
    @IBAction func clickedSun(_ sender: Any)
    {
    }
    
    @IBAction func clickedMon(_ sender: Any) {
        
    }
    
    @IBAction func clickedThe(_ sender: Any) {
        
    }
    
    @IBAction func clickedWed(_ sender: Any) {
        
    }
    
    @IBAction func clickedThu(_ sender: Any) {
        
    }
    
    @IBAction func clickedFri(_ sender: Any) {
        
    }
    
    @IBAction func clickedSat(_ sender: Any) {
        
    }
    
    //MARK:- API Calling
    
    func orederApiCall()
    {
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(strOrderID, forKey: "orderid")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_ORDER_DETAILS as NSString , type: .TYPE_POST_RAWDATA, ServiceName: GET_ORDER_DETAILS, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == GET_ORDER_DETAILS
            {
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    
                    let dic = (arrDicData?[0] as? NSDictionary)
                    print(dic ?? "")
                    self.dicOrderDetails = (dic?.mutableCopy() as? NSMutableDictionary)!
                    
                    self.setUp()
                }
              
            }
            else
            {
                
                
            }
            
        }
    }
    
    //MARK:- TableView Delegate & DataSource
    
    //MARK:- CollectionView Delegate & DataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewProduct.dequeueReusableCell(withReuseIdentifier: "ProductsDetailsCollectionCell", for: indexPath) as! ProductsDetailsCollectionCell
        
        var product_image = dicOrderDetails.value(forKey: "product_image") as? String
        product_image = product_image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(product_image ?? "")")
        cell.imgPro.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPro.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        return cell
    }
}
