//
//  WalletOffersTableCell.swift
//  DairyLux
//
//  Created by Gabani King on 06/08/21.
//

import UIKit

class WalletOffersTableCell: UITableViewCell {

    @IBOutlet weak var imgCoupon: UIImageView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    
    @IBOutlet weak var btnAppply: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
