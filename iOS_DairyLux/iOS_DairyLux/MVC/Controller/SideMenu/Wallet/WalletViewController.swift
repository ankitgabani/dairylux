//
//  WalletViewController.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 05/08/21.
//

import UIKit
import Razorpay
import Alamofire

class WalletViewController: UIViewController,  RazorpayProtocol, RazorpayPaymentCompletionProtocolWithData, responseDelegate {
    
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewApplyCode: UIView!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    var razorpay: RazorpayCheckout!
    
    var isFromHome = false
    
    var str_Amount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callGetWalletbalanceAPI()
        
        razorpay = RazorpayCheckout.initWithKey("rzp_test_tD2R2EIpRmohSh", andDelegateWithData: self)
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.viewApplyCode.layer.cornerRadius = 10
            self.viewApplyCode.layer.masksToBounds = true
            self.viewApplyCode.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        if isFromHome == true
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            appDelegate.setUpSideMenu()
        }
    }
    
    @IBAction func clickedApplyCode(_ sender: Any) {

        if txtAmount.text != ""
        {
            self.showPaymentForm(amount: self.txtAmount.text ?? "")
        }
        else
        {
            self.view.makeToast("Please enter amount")
        }
        
    }
    
    
    @IBAction func clicked100(_ sender: Any) {
        self.showPaymentForm(amount: "100")
        
        str_Amount = "100"
    }
    
    @IBAction func clicked500(_ sender: Any) {
        self.showPaymentForm(amount: "500")
        
        str_Amount = "500"
    }
    
    @IBAction func clicked700(_ sender: Any) {
        self.showPaymentForm(amount: "700")
        
        str_Amount = "700"
    }
    
    @IBAction func clicked1000(_ sender: Any) {
        self.showPaymentForm(amount: "1000")
        str_Amount = "1000"
    }
    
    @IBAction func clicked1500(_ sender: Any) {
        self.showPaymentForm(amount: "1500")
        str_Amount = "1500"
    }
    
    public func showPaymentForm(amount: String){
        let dicUser = appDelegate.dicUserLoginData
        
        let first_name = dicUser.value(forKey: "first_name") as? String
        let email = dicUser.value(forKey: "email") as? String
        let mobileno1 = dicUser.value(forKey: "mobileno1") as? String
        
        let options: [String:Any] = [
            "description": "Dhaam Natural",
            "image": UIImage(named: "11favicon")!,
            "name": first_name ?? "Ankit Gabani",
            "amount" : Double(amount)! * 100,
            "prefill": [
                "contact": mobileno1 ?? "7096859504",
                "email": email ?? "gabani7004@gmail.com"
            ],
            "theme": [
                "color": "#3A9EF0"
            ]
            
        ]
        
        if let rzp = razorpay {
            rzp.open(options)
        } else {
            print("Unable to initialize")
        }
    }
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        print("error: ", code)
        //        let storyboard = UIStoryboard(name: "payment", bundle: nil)
        //        let razorpayVC = storyboard.instantiateViewController(withIdentifier: "paymentFailedViewController") as! paymentFailedViewController
        //        self.present(razorpayVC, animated: false, completion: nil)
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        print("success: ", response)

        let paymentId = response?["razorpay_payment_id"] as! String
        
        callAddWalletTransactionAPI(transactionid: paymentId)
    }
    
    
    func callAddWalletTransactionAPI(transactionid: String)
    {
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()

        if txtAmount.text == ""
        {
            let amount = Double(str_Amount)!
            dic.setValue("\(amount)", forKey: "amount")

        }
        else
        {
            let amount = Double(txtAmount.text ?? "")!
            dic.setValue("\(amount)", forKey: "amount")
        }
                
        dic.setValue(userID, forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(transactionid, forKey: "transactionid")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + ADD_WALLET_TRANSACTION as NSString, type: .TYPE_POST_RAWDATA, ServiceName: ADD_WALLET_TRANSACTION, bodyObject: dic, delegate: self, isShowProgress: true)
    }
  
    func callGetWalletbalanceAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + WALLET_BALANCE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: WALLET_BALANCE, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == ADD_WALLET_TRANSACTION
            {
                //self.callGetWalletbalanceAPI()
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPaymentVC") as! SuccessPaymentVC
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                
            }
            else if ServiceName == WALLET_BALANCE
            {
                self.txtAmount.text = ""
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray

                if (arrDicData?.count ?? 0) > 0
                {
                    let dicData = arrDicData?[0] as? NSDictionary
                    
                    var str_new_Amount = 0.0
                    
                    if (arrDicData?.count ?? 0) > 0
                    {
                        
                        for obj in arrDicData!
                        {
                            let dicDataNew = obj as? NSDictionary
                            
                            let walletbalance = dicDataNew?.value(forKey: "creditamount") as? String
                            let debitamount = dicDataNew?.value(forKey: "debitamount") as? String
                            
                            let str_Round = Double(walletbalance ?? "")?.rounded(toPlaces: 2)
                            
                            let str_Round_Debit = Double(debitamount ?? "")?.rounded(toPlaces: 2)

                            str_new_Amount = (str_new_Amount) + (str_Round ?? 0.0) - (str_Round_Debit ?? 0.0)
                        }

                        let str_Sring = String(str_new_Amount)
                        
                        self.lblTotalAmount.text = "₹ \(str_Sring)"
                    }
                }
                
               
                
            }
            
        }
    }
 
    
}

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
