//
//  WalletOffersViewController.swift
//  DairyLux
//
//  Created by Gabani King on 05/08/21.
//

import UIKit

class WalletOffersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, responseDelegate {
    
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewApplyCode: UIView!
    @IBOutlet weak var txtEnterCode: UITextField!
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrOfferList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        callOfferListAPI()
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUp()
    {
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        DispatchQueue.main.async {
            self.viewApplyCode.layer.cornerRadius = 10
            self.viewApplyCode.layer.masksToBounds = true
            self.viewApplyCode.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedApplyCode(_ sender: Any) {
        
        if txtEnterCode.text == ""
        {
            self.view.makeToast("Please enter referal code")
        }
        else
        {
            callApply_referalcodeAPI(refer_code: txtEnterCode.text ?? "")
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfferList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "WalletOffersTableCell") as! WalletOffersTableCell
        
        let dicData = arrOfferList[indexPath.row] as? NSDictionary
        
        let couponcode = dicData?.value(forKey: "couponcode") as? String
        let descripation = dicData?.value(forKey: "descripation") as? String
        let offer_enddate = dicData?.value(forKey: "offer_enddate") as? String
        let offer_startdate = dicData?.value(forKey: "offer_startdate") as? String
        
        cell.lblCode.text = couponcode
        cell.lblDes.text = descripation
        
        cell.btnAppply.tag = indexPath.row
        cell.btnAppply.addTarget(self, action: #selector(clickedAppluCopn(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98
    }
    
    @objc func clickedAppluCopn(sender: UIButton)
    {
        let dicData = arrOfferList[sender.tag] as? NSDictionary
        let couponcode = dicData?.value(forKey: "couponcode") as? String
        callApply_referalcodeAPI(refer_code: couponcode ?? "")
    }
    
    func callApply_referalcodeAPI(refer_code: String)
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(refer_code, forKey: "refer_code")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + APPLY_REFERAL_CODE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: APPLY_REFERAL_CODE, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func callOfferListAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_OFFER_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_OFFER_LIST, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            let ReturnMessage = dicData.value(forKey: "ReturnMessage") as? String
            
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == APPLY_REFERAL_CODE
            {
                self.view.makeToast(ReturnMessage)
            }
            else if ServiceName == GET_OFFER_LIST
            {
                self.arrOfferList.removeAllObjects()
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    self.arrOfferList = (arrDicData?.mutableCopy() as? NSMutableArray)!
                }
                
                self.tblView.reloadData()
            }
        }
    }
    
}
