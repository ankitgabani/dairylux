//
//  ProductsCollectionCell.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 02/08/21.
//

import UIKit

class ProductsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgPro: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}
