//
//  ProductsDetailsVC.swift
//  DairyLux
//
//  Created by Gabani King on 02/08/21.
//

import UIKit
import SDWebImage

class ProductsDetailsVC: UIViewController, responseDelegate{
    
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var collectionViewProduct: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var lblProName: UILabel!
    @IBOutlet weak var lblNewPrice: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    
//    @IBOutlet weak var viewNewAdd: UIView!
//    @IBOutlet weak var lblCountMorning: UILabel!
//    @IBOutlet weak var viewAddSubAdd: UIView!
    
    @IBOutlet weak var lblCountMorning: UILabel!

    @IBOutlet weak var lblCountNight: UILabel!

    
    @IBOutlet weak var viewSelectMorning: UIView!
    @IBOutlet weak var viewSelectNight: UIView!
    
    @IBOutlet weak var viewDays: UIView!
    @IBOutlet weak var viewALter: UIView!
    @IBOutlet weak var viewSelectDays: UIView!
    
    @IBOutlet weak var btnDays: UIButton!
    @IBOutlet weak var btnALter: UIButton!
    @IBOutlet weak var btnSelectDays: UIButton!
    
    @IBOutlet weak var viewDaysHeightCont: NSLayoutConstraint!
    @IBOutlet weak var viewSun: UIView!
    @IBOutlet weak var imgSun: UIImageView!
    
    @IBOutlet weak var viewMon: UIView!
    @IBOutlet weak var imgMon: UIImageView!
    
    @IBOutlet weak var viewThe: UIView!
    @IBOutlet weak var imgThe: UIImageView!
    
    @IBOutlet weak var viewWed: UIView!
    @IBOutlet weak var imgWed: UIImageView!
    
    @IBOutlet weak var viewThu: UIView!
    @IBOutlet weak var imgThu: UIImageView!
    
    
    @IBOutlet weak var viewFri: UIView!
    @IBOutlet weak var imgFri: UIImageView!
    
    @IBOutlet weak var viewSat: UIView!
    @IBOutlet weak var imgSat: UIImageView!
    
    
    @IBOutlet weak var btnMorning: UIButton!
    @IBOutlet weak var btnNight: UIButton!
    
    @IBOutlet weak var txtDeliveryTime: UITextField!
    
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var txtEndData: UITextField!
    
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var viewConfirm: UIView!
    
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    
    var dicProductDetails = NSMutableDictionary()
    
    let sectionInsetsSlider = UIEdgeInsets(top: 0.0,
                                           left: 0.0,
                                           bottom: 0.0,
                                           right: 0.0)
    
    var objWhichSelection = ""
    var str_Pro_ID = ""
    var str_Day_Type_Id = "1"
    var str_Time_Type_Id = "1"
    var str_selectedday = ""

    var arrSelectedDay = NSMutableArray()
    
    var isWalletBalance = false
    
    
    var objTotlePrice = 0.0
    
    var objTotleWalletamount = 0.0
    
    
    var obj_StartDate = Date()
    var obj_EndDate = Date()
    var obj_TotalDEiffenrce = 1
    
    var flowLayoutSlider: UICollectionViewFlowLayout {
        
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsetsSlider.left * (1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 1
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 280)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        
        return _flowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewSelectMorning.isHidden = false
        viewSelectNight.isHidden = true
        
        lblCountMorning.text = "1"
        lblCountNight.text = "0"

        viewDaysHeightCont.constant = 0
        pageController.isHidden = true
        
        collectionViewProduct.delegate = self
        collectionViewProduct.dataSource = self
        self.collectionViewProduct.collectionViewLayout = flowLayoutSlider
        
        setUp()
        
        callProductDetailsAPI()
        // Do any additional setup after loading the view.
    }
    
    func setUp()
    {
//        viewConfirm.isHidden = true
//        lblTotalAmount.isHidden = true
        
        DispatchQueue.main.async {
            self.viewConfirm.layer.cornerRadius = 8
            self.viewConfirm.layer.masksToBounds = true
            self.viewConfirm.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
        
//        viewNewAdd.isHidden = false
//        viewAddSubAdd.isHidden = true
        
        let example1 = NSAttributedString(string: "₹")
        let example = NSAttributedString(string: " 35.00").withStrikeThrough(1)
        
        let priceNSMutableAttributedString1 = NSMutableAttributedString()
        priceNSMutableAttributedString1.append(example1)
        priceNSMutableAttributedString1.append(example)
        lblOldPrice.attributedText =  priceNSMutableAttributedString1
        
        viewDays.backgroundColor = PRIMARY_GARDIENT_1_COLOR
        btnDays.setTitleColor(UIColor.white, for: .normal)
        viewALter.backgroundColor = UIColor.white
        btnALter.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        viewALter.layer.borderWidth = 1
        viewALter.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        viewSelectDays.backgroundColor = UIColor.white
        btnSelectDays.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        viewSelectDays.layer.borderWidth = 1
        viewSelectDays.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        btnMorning.backgroundColor = PRIMARY_GARDIENT_1_COLOR
        btnMorning.setTitleColor(UIColor.white, for: .normal)
        btnNight.backgroundColor = UIColor.white
        btnNight.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        btnNight.layer.borderWidth = 1
        btnNight.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        viewSun.isHidden = true
        viewMon.isHidden = true
        viewThe.isHidden = true
        viewWed.isHidden = true
        viewThu.isHidden = true
        viewFri.isHidden = true
        viewSat.isHidden = true
    }
    
    //MARK:- Action Method
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedNewAdd(_ sender: Any) {
        
        viewConfirm.isHidden = false
        lblTotalAmount.isHidden = false
    }
    
    @IBAction func clickedAdded(_ sender: Any)
    {
        let objCount = Int(lblCountMorning.text ?? "")
        lblCountMorning.text = "\(objCount! + 1)"
        
        let offer_price = dicProductDetails.value(forKey: "offer_price") as? String
        let _offer_price_ = Double(offer_price ?? "")?.rounded(toPlaces: 2)
        
        let str_Amount = (_offer_price_ ?? 0.0) * (Double(Int(lblCountNight.text ?? "")!) + Double(Int(lblCountMorning.text ?? "")!))

        self.objTotlePrice = str_Amount
        
        self.lblTotalAmount.text = "₹ \(str_Amount.rounded(toPlaces: 2))"

     //   viewConfirm.isHidden = false
      //  lblTotalAmount.isHidden = false
    }
    
    @IBAction func clickedSubed(_ sender: Any)
    {
        let objCount = Int(lblCountMorning.text ?? "")
        
        if objCount == 1
        {
            lblCountMorning.text = "1"
          //  viewConfirm.isHidden = true
          //  lblTotalAmount.isHidden = true
        }
        else
        {
            lblCountMorning.text = "\(objCount! - 1)"
          //  viewConfirm.isHidden = false
          //  lblTotalAmount.isHidden = false
        }
        
        let offer_price = dicProductDetails.value(forKey: "offer_price") as? String
        let _offer_price_ = Double(offer_price ?? "")?.rounded(toPlaces: 2)
        
        let str_Amount = (_offer_price_ ?? 0.0) * (Double(Int(lblCountNight.text ?? "")!) + Double(Int(lblCountMorning.text ?? "")!))

        self.objTotlePrice = str_Amount
        
        self.lblTotalAmount.text = "₹ \(str_Amount.rounded(toPlaces: 2))"
    }
    
    @IBAction func clickedNightAdd(_ sender: Any)
    {
        let objCount = Int(lblCountNight.text ?? "")
        lblCountNight.text = "\(objCount! + 1)"
        
        let offer_price = dicProductDetails.value(forKey: "offer_price") as? String
        let _offer_price_ = Double(offer_price ?? "")?.rounded(toPlaces: 2)
        
        let str_Amount = (_offer_price_ ?? 0.0) * (Double(Int(lblCountNight.text ?? "")!) + Double(Int(lblCountMorning.text ?? "")!))
        
        self.objTotlePrice = str_Amount
        
        self.lblTotalAmount.text = "₹ \(str_Amount.rounded(toPlaces: 2))"

      //  viewConfirm.isHidden = false
      //  lblTotalAmount.isHidden = false
    }
    
    
    @IBAction func clickedNightSub(_ sender: Any)
    {
        let objCount = Int(lblCountNight.text ?? "")
        
        if objCount == 1
        {
            lblCountNight.text = "1"
         //   viewConfirm.isHidden = true
         //   lblTotalAmount.isHidden = true
        }
        else
        {
            lblCountNight.text = "\(objCount! - 1)"
         //   viewConfirm.isHidden = false
          //  lblTotalAmount.isHidden = false
        }
        
        let offer_price = dicProductDetails.value(forKey: "offer_price") as? String
        let _offer_price_ = Double(offer_price ?? "")?.rounded(toPlaces: 2)
        
        let str_Amount = (_offer_price_ ?? 0.0) * (Double(Int(lblCountNight.text ?? "")!) + Double(Int(lblCountMorning.text ?? "")!))

        self.objTotlePrice = str_Amount
        
        self.lblTotalAmount.text = "₹ \(str_Amount.rounded(toPlaces: 2))"
    }
    
    
    @IBAction func clickedDays(_ sender: Any) {
        str_Day_Type_Id = "1"
        viewDaysHeightCont.constant = 0
        
        viewSun.isHidden = true
        viewMon.isHidden = true
        viewThe.isHidden = true
        viewWed.isHidden = true
        viewThu.isHidden = true
        viewFri.isHidden = true
        viewSat.isHidden = true
        
        viewDays.backgroundColor = PRIMARY_GARDIENT_1_COLOR
        btnDays.setTitleColor(UIColor.white, for: .normal)
        
        viewALter.backgroundColor = UIColor.white
        btnALter.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        viewALter.layer.borderWidth = 1
        viewALter.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        viewSelectDays.backgroundColor = UIColor.white
        btnSelectDays.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        viewSelectDays.layer.borderWidth = 1
        viewSelectDays.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
     }
    
    @IBAction func clickedAlter(_ sender: Any)
    {
        viewDaysHeightCont.constant = 0
        str_Day_Type_Id = "2"
        
        viewSun.isHidden = true
        viewMon.isHidden = true
        viewThe.isHidden = true
        viewWed.isHidden = true
        viewThu.isHidden = true
        viewFri.isHidden = true
        viewSat.isHidden = true
        
        viewALter.backgroundColor = PRIMARY_GARDIENT_1_COLOR
        btnALter.setTitleColor(UIColor.white, for: .normal)
        
        viewDays.backgroundColor = UIColor.white
        btnDays.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        viewDays.layer.borderWidth = 1
        viewDays.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        viewSelectDays.backgroundColor = UIColor.white
        btnSelectDays.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        viewSelectDays.layer.borderWidth = 1
        viewSelectDays.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
    }
    
    @IBAction func clickedSelectDays(_ sender: Any) {
        
        viewDaysHeightCont.constant = 55.5
        str_Day_Type_Id = "3"
        
        viewSun.isHidden = false
        viewMon.isHidden = false
        viewThe.isHidden = false
        viewWed.isHidden = false
        viewThu.isHidden = false
        viewFri.isHidden = false
        viewSat.isHidden = false
        
        viewSelectDays.backgroundColor = PRIMARY_GARDIENT_1_COLOR
        btnSelectDays.setTitleColor(UIColor.white, for: .normal)
        
        viewDays.backgroundColor = UIColor.white
        btnDays.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        viewDays.layer.borderWidth = 1
        viewDays.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        
        viewALter.backgroundColor = UIColor.white
        btnALter.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
        viewALter.layer.borderWidth = 1
        viewALter.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
    }
    
    @IBAction func clickedMorning(_ sender: Any) {
        
     
        if btnMorning.backgroundColor == PRIMARY_GARDIENT_1_COLOR
        {
            viewSelectMorning.isHidden = true
            lblCountMorning.text = "0"

            btnMorning.backgroundColor = UIColor.white
            btnMorning.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            btnMorning.layer.borderWidth = 1
            btnMorning.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        }
        else
        {
            
            viewSelectMorning.isHidden = false
            lblCountMorning.text = "1"
            
            btnMorning.backgroundColor = PRIMARY_GARDIENT_1_COLOR
            btnMorning.setTitleColor(UIColor.white, for: .normal)
        }
        
        if btnMorning.backgroundColor == PRIMARY_GARDIENT_1_COLOR && btnNight.backgroundColor == PRIMARY_GARDIENT_1_COLOR
        {
            str_Time_Type_Id = "1,2"
        }
        else if btnMorning.backgroundColor == PRIMARY_GARDIENT_1_COLOR
        {
            str_Time_Type_Id = "1"
        }
        else if btnNight.backgroundColor == PRIMARY_GARDIENT_1_COLOR
        {
            str_Time_Type_Id = "2"
        }
        else
        {
            str_Time_Type_Id = ""
        }
            
        let offer_price = dicProductDetails.value(forKey: "offer_price") as? String
        let _offer_price_ = Double(offer_price ?? "")?.rounded(toPlaces: 2)

        let str_Amount = (_offer_price_ ?? 0.0) * (Double(Int(lblCountNight.text ?? "")!) + Double(Int(lblCountMorning.text ?? "")!))

        self.objTotlePrice = str_Amount
        
        self.lblTotalAmount.text = "₹ \(str_Amount.rounded(toPlaces: 2))"
        
    }
   
    @IBAction func clickedNight(_ sender: Any) {
        
        if btnNight.backgroundColor == PRIMARY_GARDIENT_1_COLOR
        {
            viewSelectNight.isHidden = true
            lblCountNight.text = "0"

            btnNight.backgroundColor = UIColor.white
            btnNight.setTitleColor(PRIMARY_GARDIENT_1_COLOR, for: .normal)
            btnNight.layer.borderWidth = 1
            btnNight.layer.borderColor = PRIMARY_GARDIENT_1_COLOR.cgColor
        }
        else
        {
            
            viewSelectNight.isHidden = false
            lblCountNight.text = "1"
            
            btnNight.backgroundColor = PRIMARY_GARDIENT_1_COLOR
            btnNight.setTitleColor(UIColor.white, for: .normal)
        }
      
        if btnMorning.backgroundColor == PRIMARY_GARDIENT_1_COLOR && btnNight.backgroundColor == PRIMARY_GARDIENT_1_COLOR
        {
            str_Time_Type_Id = "1,2"
        }
        else if btnMorning.backgroundColor == PRIMARY_GARDIENT_1_COLOR
        {
            str_Time_Type_Id = "1"
        }
        else if btnNight.backgroundColor == PRIMARY_GARDIENT_1_COLOR
        {
            str_Time_Type_Id = "2"
        }
        else
        {
            str_Time_Type_Id = ""
        }
        
        let offer_price = dicProductDetails.value(forKey: "offer_price") as? String
        let _offer_price_ = Double(offer_price ?? "")?.rounded(toPlaces: 2)

        let str_Amount = (_offer_price_ ?? 0.0) * (Double(Int(lblCountNight.text ?? "")!) + Double(Int(lblCountMorning.text ?? "")!))

        self.objTotlePrice = str_Amount
        
        self.lblTotalAmount.text = "₹ \(str_Amount.rounded(toPlaces: 2))"
        
    }
    
    @IBAction func clickedSelecteDivliveryTine(_ sender: Any)
    {
        objWhichSelection = "time"
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        present(selector, animated: true, completion: nil)
    }
    
    @IBAction func clickedSelectStartDate(_ sender: Any)
    {
        objWhichSelection = "Start_date"
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(Date())
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        present(selector, animated: true, completion: nil)
    }
    
    @IBAction func clickedSelectEndDate(_ sender: Any)
    {
        
        let alert = UIAlertController(title: "", message: "End date", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Choose Specific Date", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            
            self.objWhichSelection = "end_date"
            let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
            selector.delegate = self
            selector.optionCurrentDate =  self.singleDate
            selector.optionCurrentDates = Set( self.multipleDates)
            selector.optionCurrentDateRange.setStartDate( self.multipleDates.first ??  self.singleDate)
            selector.optionCurrentDateRange.setEndDate( self.multipleDates.last ??  self.singleDate)
            selector.optionStyles.showDateMonth(true)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showYear(true)
            selector.optionStyles.showTime(false)
            self.present(selector, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Not Yet Decided", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
        }))
        
        //uncomment for iPad Support
        //alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @IBAction func clickedConfirm(_ sender: Any)
    {
//        if txtEndData.text == ""
//        {
//            self.view.makeToast("Please select End Date.")
//        }
//        else
        
        if lblCountNight.text == "0" && lblCountMorning.text == "0"
        {
            self.view.makeToast("Please select quantity")
        }
        else if str_Day_Type_Id == "3" && arrSelectedDay.count == 0
        {
            self.view.makeToast("Please select Day")
        }
        else if self.objTotlePrice == 0.0
        {
            self.view.makeToast("Please add amount in Wallet")
        }
        else if self.objTotlePrice > self.objTotleWalletamount
        {
            self.view.makeToast("Please add amount in Wallet")
        }
        else
        {
            btnConfirm.isUserInteractionEnabled = false
            callSaveOrderAPI()
        }
       
    }
    
    @IBAction func clickedSun(_ sender: Any)
    {
        if imgSun.image == UIImage(named: "checkbox_active")
        {
            imgSun.image = UIImage(named: "checkbox_deactive")
            arrSelectedDay.remove("1")
        }
        else
        {
            imgSun.image = UIImage(named: "checkbox_active")
            arrSelectedDay.add("1")
        }
    }
    
    @IBAction func clickedMon(_ sender: Any) {
        if imgMon.image == UIImage(named: "checkbox_active")
        {
            imgMon.image = UIImage(named: "checkbox_deactive")
            arrSelectedDay.remove("2")
        }
        else
        {
            imgMon.image = UIImage(named: "checkbox_active")
            arrSelectedDay.add("2")
        }
    }
    
    @IBAction func clickedThe(_ sender: Any) {
        if imgThe.image == UIImage(named: "checkbox_active")
        {
            imgThe.image = UIImage(named: "checkbox_deactive")
            arrSelectedDay.remove("3")
        }
        else
        {
            imgThe.image = UIImage(named: "checkbox_active")
            arrSelectedDay.add("3")
        }
    }
    
    @IBAction func clickedWed(_ sender: Any) {
        if imgWed.image == UIImage(named: "checkbox_active")
        {
            imgWed.image = UIImage(named: "checkbox_deactive")
            arrSelectedDay.remove("4")
        }
        else
        {
            imgWed.image = UIImage(named: "checkbox_active")
            arrSelectedDay.add("4")
        }
    }
    
    @IBAction func clickedThu(_ sender: Any) {
        if imgThu.image == UIImage(named: "checkbox_active")
        {
            imgThu.image = UIImage(named: "checkbox_deactive")
            arrSelectedDay.remove("5")
        }
        else
        {
            imgThu.image = UIImage(named: "checkbox_active")
            arrSelectedDay.add("5")
        }
    }
    
    @IBAction func clickedFri(_ sender: Any) {
        if imgFri.image == UIImage(named: "checkbox_active")
        {
            imgFri.image = UIImage(named: "checkbox_deactive")
            arrSelectedDay.remove("6")
        }
        else
        {
            imgFri.image = UIImage(named: "checkbox_active")
            arrSelectedDay.add("6")
        }
    }
    
    @IBAction func clickedSat(_ sender: Any) {
        if imgSat.image == UIImage(named: "checkbox_active")
        {
            imgSat.image = UIImage(named: "checkbox_deactive")
            arrSelectedDay.remove("7")
        }
        else
        {
            imgSat.image = UIImage(named: "checkbox_active")
            arrSelectedDay.add("7")
        }
    }    
    
    
    //MARK:- API Calling
    func callProductDetailsAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(str_Pro_ID, forKey: "product_id")
        
        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + PRODUCT_DETAILS as NSString, type: .TYPE_POST_RAWDATA, ServiceName: PRODUCT_DETAILS, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func callSaveOrderAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let offer_price = dicProductDetails.value(forKey: "offer_price") as? String
        let _offer_price_ = Double(offer_price ?? "")?.rounded(toPlaces: 2)
        
        let str_Amount = (_offer_price_ ?? 0.0) * Double(Int(lblCountMorning.text ?? "")!)
        
        let string77 = arrSelectedDay.componentsJoined(by: ",")
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(UIDevice.current.identifierForVendor?.uuidString, forKey: "device_id")
        dic.setValue("IOS", forKey: "device_type")
        dic.setValue(str_Day_Type_Id, forKey: "daytypeid")
        dic.setValue(str_Amount, forKey: "amount")
        
        let arrData = NSMutableArray()
        
        let dicData = NSMutableDictionary()
        dicData.setValue(str_Pro_ID, forKey: "productid")
        dicData.setValue(lblCountMorning.text ?? "", forKey: "qty")
        dicData.setValue(str_Time_Type_Id, forKey: "timeid")
        dicData.setValue(txtStartDate.text ?? "", forKey: "order_startdate")
        dicData.setValue(txtEndData.text ?? "", forKey: "order_enddate")
        dicData.setValue(string77, forKey: "selectedday")
        dicData.setValue("\(lblCountMorning.text ?? "")", forKey: "morning")
        dicData.setValue("\(lblCountNight.text ?? "")", forKey: "night")
 
        arrData.add(dicData)
        
        dic.setValue(arrData, forKey: "Data")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + SAVE_ORDER as NSString, type: .TYPE_POST_RAWDATA, ServiceName: SAVE_ORDER, bodyObject: dic, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
             
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == PRODUCT_DETAILS
            {
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    let dic = (arrDicData?[0] as? NSDictionary)
                    
                    self.dicProductDetails = (dic?.mutableCopy() as? NSMutableDictionary)!
                    
                    self.setUpDetails()
                }
                
                self.callGetWalletbalanceAPI()
                                
            }
            else if ServiceName == WALLET_BALANCE
            {
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray

                if (arrDicData?.count ?? 0) > 0
                {
                    let dicData = arrDicData?[0] as? NSDictionary
                    
                    var str_new_Amount = 0.0
                    
                    if (arrDicData?.count ?? 0) > 0
                    {
                        
                        for obj in arrDicData!
                        {
                            let dicDataNew = obj as? NSDictionary
                            
                            let walletbalance = dicDataNew?.value(forKey: "creditamount") as? String
                            let debitamount = dicDataNew?.value(forKey: "debitamount") as? String
                            
                            let str_Round = Double(walletbalance ?? "")?.rounded(toPlaces: 2)
                            
                            let str_Round_Debit = Double(debitamount ?? "")?.rounded(toPlaces: 2)

                            str_new_Amount = (str_new_Amount) + (str_Round ?? 0.0) - (str_Round_Debit ?? 0.0)
                        }
                        
                        self.objTotleWalletamount = str_new_Amount
                       
                        
                    }
                }
                else
                {
                    self.isWalletBalance = false
                }
                    
                
            }
            else
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPaymentVC") as! SuccessPaymentVC
                vc.isFromOrder = true
                self.navigationController?.pushViewController(vc, animated: true)
//                self.view.makeToast("Order Created Successfully!")
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
//                    self.btnConfirm.isUserInteractionEnabled = true
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderListViewController") as! OrderListViewController
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }

            }
            
        }
    }
    
    func callGetWalletbalanceAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + WALLET_BALANCE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: WALLET_BALANCE, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func setUpDetails()
    {
        self.lblProName.text = dicProductDetails.value(forKey: "product_name") as? String
        
        self.lblDes.text = dicProductDetails.value(forKey: "description") as? String

        let product_price = dicProductDetails.value(forKey: "product_price") as? String
        let offer_price = dicProductDetails.value(forKey: "offer_price") as? String

        let _product_price_ = Double(product_price ?? "")?.rounded(toPlaces: 2)
        let _offer_price_ = Double(offer_price ?? "")?.rounded(toPlaces: 2)

        lblNewPrice.text = "₹ \(_offer_price_ ?? 0.0)"
         
        
        let example1 = NSAttributedString(string: "₹")
        let example = NSAttributedString(string: " \(_product_price_ ?? 0.0)").withStrikeThrough(1)
     
        let priceNSMutableAttributedString1 = NSMutableAttributedString()
        priceNSMutableAttributedString1.append(example1)
        priceNSMutableAttributedString1.append(example)
        lblOldPrice.attributedText =  priceNSMutableAttributedString1
        
        let str_Amount = (_offer_price_ ?? 0.0) * Double(Int(lblCountMorning.text ?? "")!)
        
        self.lblTotalAmount.text = "₹ \(str_Amount.rounded(toPlaces: 2))"
        
        self.objTotlePrice = str_Amount.rounded(toPlaces: 2)
        
        let calendar = Calendar.current
        // Use the following line if you want midnight UTC instead of local time
        //calendar.timeZone = TimeZone(secondsFromGMT: 0)
        let today = Date()
        let midnight = calendar.startOfDay(for: today)
        let tomorrow = calendar.date(byAdding: .day, value: 1, to: midnight)!
        
        txtStartDate.text = tomorrow.stringFromFormat("dd/MM/yyyy")
        
        obj_StartDate = tomorrow
        
        pageController.numberOfPages = 1
        collectionViewProduct.reloadData()
    }
}

//MARK:- UICollectionView
extension ProductsDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewProduct.dequeueReusableCell(withReuseIdentifier: "ProductsDetailsCollectionCell", for: indexPath) as! ProductsDetailsCollectionCell
        
        var product_image = dicProductDetails.value(forKey: "product_image") as? String
        product_image = product_image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(product_image ?? "")")
        cell.imgPro.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPro.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        return cell
    }
    
}

extension ProductsDetailsVC: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        
        let PevDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        
        if date >= PevDate
        {
            return true
        }
        return false
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        
        if objWhichSelection == "time"
        {
            txtDeliveryTime.text = date.stringFromFormat("hh:mm")
        }
        else if objWhichSelection == "Start_date"
        {
            obj_StartDate = date

            txtStartDate.text = date.stringFromFormat("dd/MM/yyyy")
        }
        else if objWhichSelection == "end_date"
        {
            obj_EndDate = date
            
            txtEndData.text = date.stringFromFormat("dd/MM/yyyy")
        }
       
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            if objWhichSelection == "time"
            {
                txtDeliveryTime.text = date.stringFromFormat("hh:mm")
            }
            else if objWhichSelection == "Start_date"
            {
                obj_StartDate = date

                txtStartDate.text = date.stringFromFormat("dd/MM/yyyy")
            }
            else if objWhichSelection == "end_date"
            {
                obj_EndDate = date
                txtEndData.text = date.stringFromFormat("dd/MM/yyyy")
            }
        }
        else {
            if objWhichSelection == "time"
            {
                txtDeliveryTime.text = "No Date Selected"
                
            }
            else if objWhichSelection == "Start_date"
            {
                txtStartDate.text = "No Date Selected"
                
            }
            else if objWhichSelection == "end_date"
            {
                txtEndData.text = "No Date Selected"
            }
        }
        multipleDates = dates
    }
}
