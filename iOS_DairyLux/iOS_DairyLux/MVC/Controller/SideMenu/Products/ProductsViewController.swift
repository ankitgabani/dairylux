//
//  ProductsViewController.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 02/08/21.
//

import UIKit
import SDWebImage

class ProductsViewController: UIViewController, responseDelegate {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var topImgBgHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var lblMilk: UILabel!
    @IBOutlet weak var viewMilk: UIView!
    @IBOutlet weak var lblMilkPro: UILabel!
    @IBOutlet weak var viewMilkPro: UIView!
    
    @IBOutlet weak var tblViw: UITableView!
    @IBOutlet weak var collectionViewPro: UICollectionView!
    
    var isFromHome = false
    
    var arrSubCatList = NSMutableArray()
    var arrProductList = NSMutableArray()
    
    var dicFirst = NSDictionary()
    var dicSecound = NSDictionary()
    
    var flowLayoutTrend: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 110, height: 140)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 15
        return _flowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblViw.delegate = self
        tblViw.dataSource = self
        
        collectionViewPro.delegate = self
        collectionViewPro.dataSource = self
        self.collectionViewPro.collectionViewLayout = flowLayoutTrend
        
        lblMilk.alpha = 1
        lblMilkPro.alpha = 0.5
        
        viewMilk.isHidden = false
        viewMilkPro.isHidden = true
        
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            topImgBgHieghtCont.constant = 160
        }
        else
        {
            topImgBgHieghtCont.constant = 140
        }
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        callCategoryListAPI()
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        if isFromHome == true
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            appDelegate.setUpSideMenu()
        }
    }
    
    
    @IBAction func clickedMilk(_ sender: Any) {
        
        let dicData = dicFirst
        
        callSubCategoryListAPI(category_id: (dicData.value(forKey: "id") as? String) ?? "")
        
        lblMilk.alpha = 1
        lblMilkPro.alpha = 0.5
        
        viewMilk.isHidden = false
        viewMilkPro.isHidden = true
    }
    
    @IBAction func clickedMilkPro(_ sender: Any) {
        
        let dicData = dicSecound
        
        callSubCategoryListAPI(category_id: (dicData.value(forKey: "id") as? String) ?? "")

        lblMilk.alpha = 0.5
        lblMilkPro.alpha = 1.0
        
        viewMilk.isHidden = true
        viewMilkPro.isHidden = false
    }
    
    @IBAction func clickedviewAll(_ sender: Any) {
    }
    
}

extension ProductsViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProductList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViw.dequeueReusableCell(withIdentifier: "ProductsTableCell") as! ProductsTableCell
        
        let dicData = arrProductList[indexPath.row] as? NSDictionary
        
        let product_name = dicData?.value(forKey: "product_name") as? String
        let product_price = dicData?.value(forKey: "product_price") as? String
        let offer_price = dicData?.value(forKey: "offer_price") as? String
        let product_weight = dicData?.value(forKey: "product_weight") as? String

        
        cell.viewSelectedPro.isHidden = true
        cell.viewNullPro.isHidden = false
        
        let _product_price_ = Double(product_price ?? "")?.rounded(toPlaces: 2)
        let _offer_price_ = Double(offer_price ?? "")?.rounded(toPlaces: 2)

        cell.lblNewPrice.text = "₹ \(_offer_price_ ?? 0.0)"
        
        let example1 = NSAttributedString(string: "₹")
        let example = NSAttributedString(string: " \(_product_price_ ?? 0.0)").withStrikeThrough(1)
        
        cell.lblName.text = product_name ?? ""
        cell.lblMl.text = product_weight ?? ""
        
        let priceNSMutableAttributedString1 = NSMutableAttributedString()
        priceNSMutableAttributedString1.append(example1)
        priceNSMutableAttributedString1.append(example)
        cell.lblOldPrise.attributedText =  priceNSMutableAttributedString1
        
        
        var product_image = dicData?.value(forKey: "product_image") as? String
        product_image = product_image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(product_image ?? "")")
        cell.imgPro.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPro.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        
//        cell.btnNewAddPro.tag = indexPath.row
//        cell.btnNewAddPro.addTarget(self, action: #selector(clickedAddPro(sender:)), for: .touchUpInside)
//
//        cell.btnAdd.tag = indexPath.row
//        cell.btnAdd.addTarget(self, action: #selector(clickedAdditionPro(sender:)), for: .touchUpInside)
//
//        cell.btnSub.tag = indexPath.row
//        cell.btnSub.addTarget(self, action: #selector(clickedSubtractionPro(sender:)), for: .touchUpInside)
//
//        cell.btnDeatils.tag = indexPath.row
//        cell.btnDeatils.addTarget(self, action: #selector(clickedOpenDetails(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dicData = arrProductList[indexPath.row] as? NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductsDetailsVC") as! ProductsDetailsVC
        vc.str_Pro_ID = dicData?.value(forKey: "id") as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
//    @objc func clickedAddPro(sender: UIButton)
//    {
//        let index = sender.tag
//
//        let cell = tblViw.cellForRow(at: IndexPath.init(row: index, section: 0)) as! ProductsTableCell
//        cell.viewNullPro.isHidden = true
//        cell.viewSelectedPro.isHidden = false
//
//    }
//
//    @objc func clickedAdditionPro(sender: UIButton)
//    {
//        let index = sender.tag
//
//        let cell = tblViw.cellForRow(at: IndexPath.init(row: index, section: 0)) as! ProductsTableCell
//
//        let objCount = Int(cell.lblProCount.text ?? "")
//
//        cell.lblProCount.text = "\(objCount! + 1)"
//    }
//
//    @objc func clickedSubtractionPro(sender: UIButton)
//    {
//        let index = sender.tag
//
//        let cell = tblViw.cellForRow(at: IndexPath.init(row: index, section: 0)) as! ProductsTableCell
//
//        let objCount = Int(cell.lblProCount.text ?? "")
//
//        if objCount == 1
//        {
//            cell.viewSelectedPro.isHidden = true
//            cell.viewNullPro.isHidden = false
//        }
//        else
//        {
//            cell.lblProCount.text = "\(objCount! - 1)"
//            cell.viewNullPro.isHidden = true
//            cell.viewSelectedPro.isHidden = false
//        }
//
//    }
    
//    @objc func clickedOpenDetails(sender: UIButton)
//    {
////        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductsDetailsVC") as! ProductsDetailsVC
////        self.navigationController?.pushViewController(vc, animated: true)
//    }
 
    //MARK:- API Call
    func callCategoryListAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + CATEGORY_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: CATEGORY_LIST, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func callSubCategoryListAPI(category_id: String)
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(category_id, forKey: "category_id")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + SUB_CATEGORY_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: SUB_CATEGORY_LIST, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func callProductListAPI(subcat_id: String)
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")
        dic.setValue(subcat_id, forKey: "subcat_id")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + PRODUCT_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: PRODUCT_LIST, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")

            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == CATEGORY_LIST
            {
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    self.dicFirst = (arrDicData?[0] as? NSDictionary)!
                    self.dicSecound = (arrDicData?[1] as? NSDictionary)!
                    
                    self.lblMilk.text = self.dicFirst.value(forKey: "cat_name") as? String
                    self.lblMilkPro.text = self.dicSecound.value(forKey: "cat_name") as? String
                    
                    self.callSubCategoryListAPI(category_id: (self.dicFirst.value(forKey: "id") as? String)!)
                }
            }
            else if ServiceName == SUB_CATEGORY_LIST
            {
                self.arrSubCatList.removeAllObjects()
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray

                if arrDicData!.count > 0
                {
                    self.arrSubCatList = (arrDicData?.mutableCopy() as? NSMutableArray)!
                    
                    let dicData = self.arrSubCatList[0] as? NSDictionary

                    let id = dicData?.value(forKey: "id") as? String
                    self.callProductListAPI(subcat_id: id ?? "")
                }
                
                self.collectionViewPro.reloadData()
            }
            else if ServiceName == PRODUCT_LIST
            {
                self.arrProductList.removeAllObjects()
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray

                if (arrDicData?.count ?? 0) > 0
                {
                    self.arrProductList = (arrDicData?.mutableCopy() as? NSMutableArray)!
                }
                
                self.tblViw.reloadData()
            }

        }
    }
    
    func didFinishWithFailure(ServiceName: String) {
        
    }
}

//MARK:- UICollectionView Delegate & DataSource
extension ProductsViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSubCatList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewPro.dequeueReusableCell(withReuseIdentifier: "ProductsCollectionCell", for: indexPath) as! ProductsCollectionCell
        
        let dicData = arrSubCatList[indexPath.row] as? NSDictionary
        
        let subcat_name = dicData?.value(forKey: "subcat_name") as? String

        var product_image = dicData?.value(forKey: "cat_image") as? String
        product_image = product_image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(product_image ?? "")")
        cell.imgPro.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPro.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        cell.lblName.text = subcat_name ?? ""
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let dicData = arrSubCatList[indexPath.row] as? NSDictionary
        
        let id = dicData?.value(forKey: "id") as? String
        callProductListAPI(subcat_id: id ?? "")
        self.tblViw.reloadData()
    }
}

extension NSAttributedString {
    
    /// Returns a new instance of NSAttributedString with same contents and attributes with strike through added.
    /// - Parameter style: value for style you wish to assign to the text.
    /// - Returns: a new instance of NSAttributedString with given strike through.
    func withStrikeThrough(_ style: Int = 1) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(attributedString: self)
        attributedString.addAttribute(.strikethroughStyle,
                                      value: style,
                                      range: NSRange(location: 0, length: string.count))
        return NSAttributedString(attributedString: attributedString)
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
