//
//  ProductsTableCell.swift
//  DairyLux
//
//  Created by Mac MIni M1 on 02/08/21.
//

import UIKit

class ProductsTableCell: UITableViewCell {

    @IBOutlet weak var imgPro: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMl: UILabel!
    
    @IBOutlet weak var lblNewPrice: UILabel!
    @IBOutlet weak var lblOldPrise: UILabel!
    
    @IBOutlet weak var viewNullPro: UIView!
    @IBOutlet weak var viewSelectedPro: UIView!
    
    @IBOutlet weak var btnNewAddPro: UIButton!
    @IBOutlet weak var btnSub: UIButton!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblProCount: UILabel!
    @IBOutlet weak var btnDeatils: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
