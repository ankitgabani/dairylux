//
//  HomeViewController.swift
//  DairyLux
//
//  Created by Gabani King on 31/07/21.
//

import UIKit
import LGSideMenuController

class HomeViewController: UIViewController,responseDelegate {

    //MARK:- IBOutlet
    @IBOutlet weak var collectionViewHeightCont: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var TopImageHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var lblWalletBalance: UILabel!
    @IBOutlet weak var collectionViewBanner: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!

    @IBOutlet weak var viewBrowersPro: UIView!
    
    @IBOutlet weak var viewTopHeightCont: NSLayoutConstraint!
    @IBOutlet weak var viewBototmHeightCont: NSLayoutConstraint!

    @IBOutlet weak var mainViewBottom: UIView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblFirstView: UILabel!
    @IBOutlet weak var lblSecoundView: UILabel!
    @IBOutlet weak var lblThirdView: UILabel!
    @IBOutlet weak var lblFourView: UILabel!
    
    var timerRR: Timer?
    

    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 0.0,
                                     bottom: 0.0,
                                     right: 0.0)
    let itemsPerRow: CGFloat = 1
    
    var flowLayout: UICollectionViewFlowLayout {
        
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: self.collectionViewBanner.frame.size.width, height: 190)
        }
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0.0
        
        return _flowLayout
        
    }
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
       let dicUser = appDelegate.dicUserLoginData
        
        let first_name = dicUser.value(forKey: "first_name") as? String
        
        lblName.text = "Hey \(first_name ?? ""),"
        
        viewTopHeightCont.constant = (UIScreen.main.bounds.width - 45) / 2
        viewBototmHeightCont.constant = (UIScreen.main.bounds.width - 45) / 2

        setUp()
        setUpattributedString()
        setUpattributedString1()
        setUpattributedString2()
        setUpattributedString3()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.callGetWalletbalanceAPI()
        
        self.navigationController?.navigationBar.isHidden = true

        timerRR?.invalidate()
        timerRR = nil
        self.pageController.numberOfPages = 3
        self.startTimer()
    }
    
    func setUpattributedString()
    {
        lblFirstView.numberOfLines = 0
        
        let attributedString1 = NSMutableAttributedString(string: "Farm to home\nin", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
      
        let attributedString2 = NSMutableAttributedString(string: "\n36 ", attributes: [
            .font: UIFont(name: "NunitoSans-ExtraBold", size: 23.0)!,
            .foregroundColor: UIColor.white
        ])
        
        let attributedString3 = NSMutableAttributedString(string: "hours", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
        
        let priceNSMutableAttributedString = NSMutableAttributedString()
        priceNSMutableAttributedString.append(attributedString1)
        priceNSMutableAttributedString.append(attributedString2)
        priceNSMutableAttributedString.append(attributedString3)

        lblFirstView.attributedText = priceNSMutableAttributedString
        
        lblSecoundView.numberOfLines = 0
        
        let attributedString11 = NSMutableAttributedString(string: "Fresh", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
      
        let attributedString21 = NSMutableAttributedString(string: "\nCow & Buffalo", attributes: [
            .font: UIFont(name: "NunitoSans-ExtraBold",  size: 20)!,
            .foregroundColor: UIColor.white
        ])
        
        let attributedString31 = NSMutableAttributedString(string: "\nMilk", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
        
        let priceNSMutableAttributedString1 = NSMutableAttributedString()
        priceNSMutableAttributedString1.append(attributedString11)
        priceNSMutableAttributedString1.append(attributedString21)
        priceNSMutableAttributedString1.append(attributedString31)
        lblSecoundView.attributedText = priceNSMutableAttributedString1
    }
    
    func setUpattributedString1()
    {
        lblSecoundView.numberOfLines = 0
        
        let attributedString11 = NSMutableAttributedString(string: "Fresh", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
      
        let attributedString21 = NSMutableAttributedString(string: "\nCow & Buffalo", attributes: [
            .font: UIFont(name: "NunitoSans-ExtraBold",  size: 20)!,
            .foregroundColor: UIColor.white
        ])
        
        let attributedString31 = NSMutableAttributedString(string: "\nMilk", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
        
        let priceNSMutableAttributedString1 = NSMutableAttributedString()
        priceNSMutableAttributedString1.append(attributedString11)
        priceNSMutableAttributedString1.append(attributedString21)
        priceNSMutableAttributedString1.append(attributedString31)
        lblSecoundView.attributedText = priceNSMutableAttributedString1
    }
    
    func setUpattributedString2()
    {
        lblThirdView.numberOfLines = 0
        
        let attributedString11 = NSMutableAttributedString(string: "No", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
      
        let attributedString21 = NSMutableAttributedString(string: "\nAny Middle men", attributes: [
            .font: UIFont(name: "NunitoSans-ExtraBold",  size: 20)!,
            .foregroundColor: UIColor.white
        ])
        
        let attributedString31 = NSMutableAttributedString(string: "\nEver", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
        
        let priceNSMutableAttributedString1 = NSMutableAttributedString()
        priceNSMutableAttributedString1.append(attributedString11)
        priceNSMutableAttributedString1.append(attributedString21)
        priceNSMutableAttributedString1.append(attributedString31)
        lblThirdView.attributedText = priceNSMutableAttributedString1
    }
    
    func setUpattributedString3()
    {
        lblFourView.numberOfLines = 0
        
        let attributedString11 = NSMutableAttributedString(string: "100%", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
      
        let attributedString21 = NSMutableAttributedString(string: "\nQuality & Secure", attributes: [
            .font: UIFont(name: "NunitoSans-ExtraBold",  size: 20)!,
            .foregroundColor: UIColor.white
        ])
        
        let attributedString31 = NSMutableAttributedString(string: "\nDelivery", attributes: [
            .font: UIFont(name: "NunitoSans-Bold",  size: 16)!,
            .foregroundColor: UIColor.white
        ])
        
        let priceNSMutableAttributedString1 = NSMutableAttributedString()
        priceNSMutableAttributedString1.append(attributedString11)
        priceNSMutableAttributedString1.append(attributedString21)
        priceNSMutableAttributedString1.append(attributedString31)
        lblFourView.attributedText = priceNSMutableAttributedString1
    }
    
    func setUp()
    {
        if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
            TopImageHieghtCont.constant = 120
        }
        else
        {
            TopImageHieghtCont.constant = 100
        }
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 25
        mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        self.collectionViewBanner.collectionViewLayout = flowLayout
        collectionViewBanner.delegate = self
        collectionViewBanner.dataSource = self
        
        DispatchQueue.main.async {
            self.viewBrowersPro.layer.cornerRadius = 35
            self.viewBrowersPro.layer.masksToBounds = true
            self.viewBrowersPro.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
       
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
     }
    
    //MARK:- IBAction
    @IBAction func clickedMenu(_ sender: Any) {
        self.sideMenuController?.showLeftView(animated: true, completion: nil)
    }
    
    @IBAction func clickedWallet(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        vc.isFromHome = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedPro(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
        vc.isFromHome = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func startTimer() {
        
        timerRR = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
    }
    
    /**
     Scroll to Next Cell
     */
    @objc func scrollToNextCell(){
        
        if pageController.currentPage == pageController.numberOfPages - 1 {
            pageController.currentPage = 0
        } else {
            pageController.currentPage += 1
        }
        
        collectionViewBanner.scrollToItem(at: IndexPath(row: pageController.currentPage, section: 0), at: .right, animated: true)
    }
    
    //MARK:- API Call
    func callGetProfileAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + GET_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: GET_PROFILE, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func callGetWalletbalanceAPI()
    {
        
        let userID = UserDefaults.standard.value(forKey: "UserID") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(userID ?? "", forKey: "userid")
        dic.setValue(appDelegate.getCurrentUserToken(), forKey: "token")

        print(dic)
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + WALLET_BALANCE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: WALLET_BALANCE, bodyObject: dic, delegate: self, isShowProgress: false)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        DispatchQueue.main.async {
            
            print(Response)
            
            let dicData = Response as! NSDictionary
            
            let token = dicData.value(forKey: "token") as? String
            appDelegate.saveCurrentUserToken(str: token ?? "")
            
            if token == "" || token == nil
            {
                appDelegate.setUpLoginData()
            }
            else if ServiceName == GET_PROFILE
            {
                let arrDicData = dicData.value(forKey: "Data") as? NSArray
                
                if (arrDicData?.count ?? 0) > 0
                {
                    let dicData = arrDicData?[0] as? NSDictionary
                    
                    let first_name = dicData?.value(forKey: "first_name") as? String 

                    self.lblName.text = "\(first_name ?? "")"
                }
                
                
            }
            else if ServiceName == WALLET_BALANCE
            {
                
                let arrDicData = dicData.value(forKey: "Data") as? NSArray

                if (arrDicData?.count ?? 0) > 0
                {
                    let dicData = arrDicData?[0] as? NSDictionary
                    
                    var str_new_Amount = 0.0
                    
                    if (arrDicData?.count ?? 0) > 0
                    {
                        
                        for obj in arrDicData!
                        {
                            let dicDataNew = obj as? NSDictionary
                            
                            let walletbalance = dicDataNew?.value(forKey: "creditamount") as? String
                            let debitamount = dicDataNew?.value(forKey: "debitamount") as? String
                            
                            let str_Round = Double(walletbalance ?? "")?.rounded(toPlaces: 2)
                            
                            let str_Round_Debit = Double(debitamount ?? "")?.rounded(toPlaces: 2)

                            str_new_Amount = (str_new_Amount) + (str_Round ?? 0.0) - (str_Round_Debit ?? 0.0)
                        }

                        let str_Sring = String(str_new_Amount)
                        
                        self.lblWalletBalance.text = "₹ \(str_Sring)"
                    }
                }
                
            }
            
        }
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
 
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewBanner.dequeueReusableCell(withReuseIdentifier: "HomeBannerCollectionCell", for: indexPath) as! HomeBannerCollectionCell
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let page = scrollView.contentOffset.x/scrollView.frame.size.width
        self.pageController.currentPage = (Int)(page)
    }
}
