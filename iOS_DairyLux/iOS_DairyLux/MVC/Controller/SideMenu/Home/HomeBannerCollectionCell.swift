//
//  HomeBannerCollectionCell.swift
//  DairyLux
//
//  Created by Gabani King on 31/07/21.
//

import UIKit

class HomeBannerCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgBanner: UIImageView!
}
