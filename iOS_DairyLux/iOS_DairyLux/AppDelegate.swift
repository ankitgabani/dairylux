//
//  AppDelegate.swift
//  iOS_DairyLux
//
//  Created by Gabani King on 26/07/21.
//

import UIKit
import IQKeyboardManager
import GoogleMaps
import GooglePlaces
import LGSideMenuController
import Firebase
import FirebaseCore
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var dicUserLoginData = NSDictionary()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        
        IQKeyboardManager.shared().isEnabled = true
        
        GMSServices.provideAPIKey(PROVIDE_API_KEY)
        
        GMSPlacesClient.provideAPIKey(PROVIDE_API_KEY)
        
        if let userLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        {
            if userLogin == true
            {
                dicUserLoginData = getCurrentUserData()

                setUpSideMenu()
            }
        }
        
        return true
    }
    
    func setUpSideMenu(){
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        let sideMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: sideMenuVC, rightViewController: nil)
        controller.navigationController?.navigationBar.isHidden = true
        controller.leftViewWidth = UIScreen.main.bounds.width - 80
        
        self.window?.rootViewController = controller
        self.window?.makeKeyAndVisible()
    }
    
    func setUpLoginData(){
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationController?.navigationBar.isHidden = true
        self.window?.rootViewController = homeNavigation
        self.window?.makeKeyAndVisible()
    }
    
    func saveCurrentUserToken(str: String)
    {
        UserDefaults.standard.setValue(str, forKey: "currentUserToken")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserToken() -> String
    {
        if let data = UserDefaults.standard.object(forKey: "currentUserToken"){
            
            return (data as? String)!
        }
        
        return ""
    }
    
    
    func saveCurrentUserData(dic: NSDictionary)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "currentUserData")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> NSDictionary
    {
        if let data = UserDefaults.standard.object(forKey: "currentUserData"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! NSDictionary
        }
        return [:]
    }
    
}

