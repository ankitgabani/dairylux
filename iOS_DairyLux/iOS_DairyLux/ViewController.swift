//
//  ViewController.swift
//  iOS_DairyLux
//
//  Created by Gabani King on 26/07/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            
            if let userLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
            {
                if userLogin == true
                {
                    appDelegate.setUpSideMenu()
                }
                else
                {
                    appDelegate.setUpLoginData()
                }
            }
            else
            {
                appDelegate.setUpLoginData()
            }
        })
        
        // Do any additional setup after loading the view.
    }


}

